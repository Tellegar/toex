package com.tellegar.toex_old;

import com.diogonunes.jcolor.Attribute;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.regex.Matcher;

import static com.diogonunes.jcolor.Ansi.colorize;
import static org.junit.jupiter.api.Assertions.*;

class StatDescriptionsTest {
	private static StatDescriptions stat_descriptions;
	
	@BeforeAll
	static void set_up() throws IOException {
		List<Description> descriptions = Description.extract(Path.of("export/ggpk/metadata/statdescriptions/stat_descriptions.txt"));
		stat_descriptions = new StatDescriptions(descriptions);
	}

//	@Test
//	public void get_hash() {
//		Map<String, List<Description.Generator>> map = new HashMap<>();
////		class Entry {
////			String hash;
////			List<Description.Generator> generators;
////		}
////		List<Entry> map = new ArrayList<>();
//
//		for (var description : stats.descriptions) {
//			for (var generator : description.generators) {
//				if (Stats.english_only && !generator.language.isEmpty()) continue;
//				if (Stats.supports_arguments(Arrays.asList(generator.arguments))) continue;
//
//				String hash = generator.format_string.replaceAll("\\{.+?[}\\]]", "{}");
//
////				var index = IntStream.range(0, map.size()).filter(i -> map.get(i).hash.equals(hash)).findFirst();
////				if (index.isEmpty()) {
////					var e = new Entry();
////					e.hash = hash;
////					e.generators = new ArrayList<>();
////					e.generators.add(generator);
////					map.add(e);
////				} else
////					map.get(index.getAsInt()).generators.add(generator);
//
//				if (map.containsKey(hash))
//					map.get(hash).add(generator);
//				else {
//					List<Description.Generator> list = new ArrayList<>();
//					list.add(generator);
//					map.put(hash, list);
//				}
//
//				nop();
//			}
//		}
//
////		mods.add(new Entry("-27 to maximum Sockets", Entry.MS(
////			"local_maximum_sockets_+", "-27"
////		)));
//
////		var x = map.stream().filter(e -> e.generators.size() > 1).toArray();
//		for (var e : map.entrySet()) {
//			String hash = e.getKey();
//			List<Description.Generator> list = e.getValue();
//			if (list.size() > 1) {
//				var x = list.stream().map(Description.Generator::get_description).distinct();
////				System.out.println(x.count() + ": " + hash);
//				if (x.count() == 1) {
//					System.out.println(hash);
//				}
//				nop();
//			}
//		}
//		nop();
//	}

//	@Test
//	public void get_hash() {
//		for (var description : stats.descriptions) {
//			for (var generator : description.generators) {
////				if (!generator.language.isEmpty()) continue; // english only
//				if (Stats.supports_arguments(Arrays.asList(generator.arguments))) continue;
//
//				String s = generator.format_string;
//				s = s.replaceAll("\\{.+?[}\\]]", "123");
//				assertEquals(Stats.get_hash(generator.format_string), Stats.get_hash(s));
//				if (!Stats.get_hash(s).equals(Stats.get_hash(generator.format_string))) {
//					System.out.println(generator.format_string);
//					fail();
//				}
//			}
//		}
//	}
	
	@Test
	public void parse_variable_scope_test() {
		class Entry {
			final String scope_str;
			final StatDescriptions.Scope scope;
			
			Entry(String scope, Integer low, Integer high) {
				this.scope_str = scope;
				this.scope = StatDescriptions.Range.of(low, high);
			}
			
			Entry(String scope, int not_value) {
				this.scope_str = scope;
				this.scope = StatDescriptions.NotValue.of(not_value);
			}
		}
		
		List<Entry> entries = new ArrayList<>();
		entries.add(new Entry("21|40", 21, 40));
		entries.add(new Entry("#|99", null, 99));
		entries.add(new Entry("#|-1", null, -1));
		entries.add(new Entry("-10", -10, -10));
		entries.add(new Entry("#|-2", null, -2));
		entries.add(new Entry("100|#", 100, null));
		entries.add(new Entry("10|#", 10, null));
		entries.add(new Entry("#|-11", null, -11));
		entries.add(new Entry("81|100", 81, 100));
		entries.add(new Entry("#|-10", null, -10));
		entries.add(new Entry("1|#", 1, null));
		entries.add(new Entry("#|1", null, 1));
		entries.add(new Entry("5|#", 5, null));
		entries.add(new Entry("10", 10, 10));
		entries.add(new Entry("11", 11, 11));
		entries.add(new Entry("12", 12, 12));
		entries.add(new Entry("#|60", null, 60));
		entries.add(new Entry("13", 13, 13));
		entries.add(new Entry("#", null, null));
		entries.add(new Entry("14", 14, 14));
		entries.add(new Entry("-1", -1, -1));
		entries.add(new Entry("15", 15, 15));
		entries.add(new Entry("16", 16, 16));
		entries.add(new Entry("1|1|#", 1, null)); // probably an error in descriptions.txt
		entries.add(new Entry("17", 17, 17));
		entries.add(new Entry("19", 19, 19));
		entries.add(new Entry("!0", 0));
		entries.add(new Entry("0", 0, 0));
		entries.add(new Entry("1", 1, 1));
		entries.add(new Entry("2", 2, 2));
		entries.add(new Entry("200", 200, 200));
		entries.add(new Entry("2|99", 2, 99));
		entries.add(new Entry("3", 3, 3));
		entries.add(new Entry("4", 4, 4));
		entries.add(new Entry("5", 5, 5));
		entries.add(new Entry("99|100", 99, 100));
		entries.add(new Entry("6", 6, 6));
		entries.add(new Entry("7", 7, 7));
		entries.add(new Entry("8", 8, 8));
		entries.add(new Entry("20|35", 20, 35));
		entries.add(new Entry("9", 9, 9));
		entries.add(new Entry("61|#", 61, null));
		entries.add(new Entry("25|#", 25, null));
		entries.add(new Entry("21", 21, 21));
		entries.add(new Entry("22", 22, 22));
		entries.add(new Entry("23", 23, 23));
		entries.add(new Entry("24", 24, 24));
		entries.add(new Entry("25", 25, 25));
		entries.add(new Entry("-9|#", -9, null));
		entries.add(new Entry("1000", 1000, 1000));
		entries.add(new Entry("61|80", 61, 80));
		entries.add(new Entry("-1|1", -1, 1));
		entries.add(new Entry("1|100", 1, 100));
		entries.add(new Entry("101|#", 101, null));
		entries.add(new Entry("1|99", 1, 99));
		entries.add(new Entry("15|24", 15, 24));
		entries.add(new Entry("0|#", 0, null));
		entries.add(new Entry("2|#", 2, null));
		entries.add(new Entry("99|#", 99, null));
		entries.add(new Entry("50|75", 50, 75));
		entries.add(new Entry("30", 30, 30));
		entries.add(new Entry("1001|#", 1001, null));
		entries.add(new Entry("4000", 4000, 4000));
		entries.add(new Entry("41|60", 41, 60));
		entries.add(new Entry("10|19", 10, 19));
		entries.add(new Entry("100", 100, 100));
		entries.add(new Entry("1|20", 1, 20));
		entries.add(new Entry("20|#", 20, null));
		entries.add(new Entry("-99|-1", -99, -1));
		entries.add(new Entry("0|99", 0, 99));
		entries.add(new Entry("#|-100", null, -100));
		for (Entry e : entries) {
			StatDescriptions.Scope scope = StatDescriptions.parse_variable_scope(e.scope_str);
			assertEquals(e.scope, scope, "scopes do not match");
		}
		
		for (var description : stat_descriptions.descriptions)
			for (var generator : description.generators)
				for (var scope : generator.variable_scopes)
					StatDescriptions.parse_variable_scope(scope);
	}

//	@Test
//	public void parse_format_pattern_test() {
//		Set<String> format_patterns = new HashSet<>();
//		for (var description : stats.descriptions) {
//			for (var generator : description.generators) {
//				var matcher = Stats.format_pattern_pattern.matcher(generator.format_string);
//				while (matcher.find())
//					format_patterns.add(matcher.group());
//			}
//		}
//
//		boolean first = true;
//		for (String format_pattern : format_patterns) {
//			if (first) first = false;
//			else System.out.print(" ");
//			System.out.print(format_pattern);
//			System.out.flush();
//		}
//		System.out.println();
//
//		for (String format_pattern : format_patterns)
//			Stats.parse_format_pattern(format_pattern);
//	}
	
	@Test
	public void match_modifier_fixed() {
		class Entry {
			final String modifier;
			final List<Map<String, StatDescriptions.Scope>> mod_stats_list;
			
			@SafeVarargs
			Entry(String modifier, Map<String, StatDescriptions.Scope>... mod_stats_list) {
				this.modifier = modifier;
				this.mod_stats_list = Arrays.asList(mod_stats_list);
			}
			
			Entry(String modifier) {
				this.modifier = modifier;
				this.mod_stats_list = null;
			}
			
			static Map<String, StatDescriptions.Scope> MS(Object... a) {
				assert a.length % 2 == 0 : "got odd number of arguments";
				Map<String, StatDescriptions.Scope> mod_stats = new HashMap<>();
				for (int i = 0; i < a.length; i += 2) {
					String stat = (String) a[i], scope = (String) a[i + 1];
					mod_stats.put(stat, StatDescriptions.parse_variable_scope(scope));
				}
				return mod_stats;
			}
		}
		List<Entry> mods = new ArrayList<>();
		
		mods.add(new Entry("+9 to Strength", Entry.MS(
			"additional_strength", "9"
		)));
		mods.add(new Entry("Rare and Unique Monsters in Area are Possessed by 2 to 3 Tormented Spirits and their Minions are Touched", Entry.MS(
			"memory_line_minimum_possessions_of_rare_unique_monsters", "2",
			"memory_line_maximum_possessions_of_rare_unique_monsters", "3"
		)));
		mods.add(new Entry("Bathed in the blood of 529 sacrificed in the name of Xibaqua\nPassives in radius are Conquered by the Vaal", Entry.MS(
			"local_unique_jewel_alternate_tree_version", "1",
			"local_unique_jewel_alternate_tree_seed", "529",
			"local_unique_jewel_alternate_tree_keystone", "1",
			"local_unique_jewel_alternate_tree_internal_revision", "#"
		)));
		mods.add(new Entry("Historic", Entry.MS(
			"local_is_alternate_tree_jewel", "#"
		)));
		mods.add(new Entry("Split Arrow fires an additional arrow", Entry.MS(
			"split_arrow_number_of_additional_arrows", "#|1"
		)));
		mods.add(new Entry("Split Arrow fires 2 additional arrows", Entry.MS(
			"split_arrow_number_of_additional_arrows", "2"
		)));
		mods.add(new Entry("47% increased Explicit Ailment Modifier magnitudes", Entry.MS(
			"heist_enchantment_ailment_mod_effect_+%", "47"
		)));
		mods.add(new Entry("47% reduced Explicit Ailment Modifier magnitudes", Entry.MS(
			"heist_enchantment_ailment_mod_effect_+%", "-47"
		)));
		mods.add(new Entry("-27 to maximum Sockets", Entry.MS(
			"local_maximum_sockets_+", "-27"
		)));
		mods.add(new Entry("Attacks cause Bleeding", Entry.MS(
			"cannot_cause_bleeding", "0",
			"bleed_on_hit_with_attacks_%", "#",
			"global_bleed_on_hit", "!0"
		), Entry.MS(
			"cannot_cause_bleeding", "0",
			"bleed_on_hit_with_attacks_%", "100|#",
			"global_bleed_on_hit", "0"
		)));
		mods.add(new Entry("+10 to maximum Sockets", Entry.MS(
			"local_maximum_sockets_+", "10"
		)));
		mods.add(new Entry("-5 to maximum Sockets", Entry.MS(
			"local_maximum_sockets_+", "-5"
		)));
		mods.add(new Entry("Breach Splinters have 0.24% chance to drop as Breachstones instead", Entry.MS(
			"map_breach_splinters_drop_as_stones_permyriad", "24"
		)));
		
		for (var e : mods) {
			var list = stat_descriptions.match_modifier(e.modifier);
			
			System.out.println("* " + e.modifier);
			for (var mod_stats : list) {
				for (var entry : mod_stats.entrySet())
					System.out.println(entry.getKey() + ": " + entry.getValue());
				System.out.println();
			}
			System.out.println();
			
			if (e.mod_stats_list != null)
				assertEquals(e.mod_stats_list, list, "\"" + e.modifier + "\"");
			else
				fail();
		}
	}
	
	// TODO test based on mods.dat
	//  there are specific modifiers with given min/max values
	//  eg. CannotBeSlowedBelowValueBosses <6472> (-30, -30)
	//      Action Speed cannot be modified to below {0}% of base value
	//      Action Speed cannot be modified to below 70% of base value
	
//	@RepeatedTest(10)
	@Test
	public void match_modifier_random() {
		Random rand = new Random();
		int seed = rand.nextInt();
//		seed = 47;
		rand.setSeed(seed);
		System.out.println("seed: " + seed);
		
		for (var description : stat_descriptions.descriptions) {
			for (var generator : description.generators) {
				if (StatDescriptions.english_only && !generator.language.isEmpty()) continue;
				
				var expected_variables = new StatDescriptions.Scope[description.variables.length];
				for (int i = 0; i < generator.variable_scopes.length; i++)
					expected_variables[i] = StatDescriptions.parse_variable_scope(generator.variable_scopes[i]);
				
				String fs = generator.format_string;
//				fs = Pattern.quote(fs)
				
				StatDescriptions.Scope[] used_variables = new StatDescriptions.Scope[expected_variables.length];
				
				Matcher m = StatDescriptions.format_p.matcher(fs);
				while (m.find()) {
					StatDescriptions.FormatPattern fp = StatDescriptions.parse_format_pattern(m.group());
					int value = 0;
					if (expected_variables[fp.index] instanceof StatDescriptions.Range range) {
						if (range.low == null && range.high == null) {
							range.low = "+d".equals(fp.argument) ? 0 : -100;
							range.high = 100;
						} else if (range.low == null)
							range.low = range.high - 99;
						else if (range.high == null)
							range.high = range.low + 100;
						
						value = rand.nextInt(range.low, range.high + 1);
					} else if (expected_variables[fp.index] instanceof StatDescriptions.NotValue not_value)
						value = rand.nextInt(not_value.value + 1, not_value.value + 100);
					else fail("variable type not supported");
					expected_variables[fp.index] = StatDescriptions.Range.of(value);
					used_variables[fp.index] = expected_variables[fp.index];
				}
				
				StatDescriptions.apply_arguments(used_variables, generator.arguments, false);
				
				fs = StatDescriptions.format_p.matcher(fs).replaceAll(match_result -> {
					StatDescriptions.FormatPattern fp = StatDescriptions.parse_format_pattern(match_result.group());
					StatDescriptions.Scope value = used_variables[fp.index];
					if (fp.argument != null && fp.argument.startsWith("+") && value.get_value() >= 0)
						return "+" + value;
					return value.toString();
				});
				
				HashMap<String, StatDescriptions.Scope> expected_mod_stats = new HashMap<>();
				for (int i = 0; i < expected_variables.length; i++)
					expected_mod_stats.put(description.variables[i], expected_variables[i]);
				
				if (generator.language.isEmpty())
					System.out.println(fs);
				
				var list = stat_descriptions.match_modifier(fs);
				if (list != null) {
					int count = 0;
					for (var mod_stats : list) {
						boolean equals = expected_mod_stats.equals(mod_stats);
						Attribute color = equals ? Attribute.TEXT_COLOR(0, 255, 0) : Attribute.TEXT_COLOR(255, 0, 0);
						if (generator.language.isEmpty())
							System.out.println(colorize("equals: " + equals, color));
						if (equals)
							count++;
					}
					assertEquals(count, 1, "only one mod-stat should be equal to expected");
				} else
					fail("no mod_stat found");
				if (generator.language.isEmpty())
					System.out.println();
			}
		}
	}
}