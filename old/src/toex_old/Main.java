package com.tellegar.toex_old;

import com.google.gson.JsonObject;
import lc.kra.system.keyboard.event.GlobalKeyEvent;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

import static com.diogonunes.jcolor.Ansi.colorize;

public class Main implements KeyListener.Hook {
	static Map<Integer, String> vk_name;
	
	static {
		vk_name = new HashMap<>();
		for (var f : GlobalKeyEvent.class.getDeclaredFields()) {
			int modifiers = f.getModifiers();
			if (Modifier.isPublic(modifiers) &&
				Modifier.isStatic(modifiers) &&
				Modifier.isFinal(modifiers) &&
				f.getType() == int.class) {
				try {
					vk_name.put(f.getInt(null), f.getName());
				} catch (IllegalAccessException e) {
					throw new RuntimeException(e);
				}
			}
		}
	}
	
	static String event_to_string(GlobalKeyEvent event) {
		if (vk_name.containsKey(event.getVirtualKeyCode())) {
			String name = vk_name.get(event.getVirtualKeyCode());
			String[] arr = event.toString().split("\\s", 2);
			return name + " " + arr[1];
		} else return event.toString();
	}
	
	static void clear_terminal_screen() {
		try {
			new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	List<Integer> arr = List.of(5, 4, 6, 1, 0, 1);
	void print_arr() {
		console.clear();
		for (int i = 0; i < arr.size(); i++) {
//			String out = arr.get(i).toString();
//			if (selected == i)
//				out = colorize(out, Attribute.BACK_COLOR(255, 255, 255));
			if (selected == i)
				console.color_back(255, 255, 255);
			console.out.println(arr.get(i).toString());
			if (selected == i)
				console.color_reset();
		}
	}
	
	static class Selection {}
	ArrayList<Selection> selection = new ArrayList<>();
	int selected = 0;
	
	GlobalKeyEvent last_key_event;
	@Override
	public void keyPressed(GlobalKeyEvent event) {
		size = console.get_size();
//		if (last_key_event != null && last_key_event.toString().equals(event.toString())) {
//			last_key_event = event;
//			return;
//		}
		last_key_event = event;
		if (event.getVirtualKeyCode() == GlobalKeyEvent.VK_UP)
			selected--;
		if (event.getVirtualKeyCode() == GlobalKeyEvent.VK_DOWN)
			selected++;
		
		render();
		console.move_to(1, size.height() - 1);
		console.out.print(event_to_string(event));
		console.out.print("\033[G");
	}
	
	static Console.Size size;
	static Console console;
	
	//	Build build = new Build();
	JsonObject json_character = null;
	
	List<String> mods = List.of(
		"+23 to maximum Life (implicit)",
		"Adds 16 to 29 Fire Damage to Attacks",
		"+54 to maximum Life",
		"+12% to Cold Resistance",
		"Gain 1 Mana per Enemy Killed",
		"+24 to Strength and Intelligence (crafted)"
	);
	
	Main() throws FileNotFoundException {
		json_character = Utilities.loadJSON("character.json").getAsJsonObject()
			.get("character").getAsJsonObject();
	}
	
	void render() {
		console.clear();
		selected = Math.clamp(selected, 0, mods.size() - 1);
		console.out.println("selected: " + selected);
		
		console.color_text(255, 255, 255);
		console.color_back(20, 21, 22);
		for (int i = 0; i < mods.size(); i++) {
			if (i == selected)
				console.out.print("\033[7m");
			console.out.println(mods.get(i));
			if (i == selected)
				console.out.print("\033[27m");
		}
		console.color_reset();
	}
	
	public static void main(String[] args) throws InterruptedException, IOException {
		console = new Console();

//		console.cursor_show(false);
		KeyListener keyListener = new KeyListener();
		console.out.println("hello world");
		keyListener.run(new Main());
		console.close();
	}
}