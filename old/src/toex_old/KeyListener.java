package com.tellegar.toex_old;

import lc.kra.system.keyboard.GlobalKeyboardHook;
import lc.kra.system.keyboard.event.GlobalKeyEvent;
import lc.kra.system.keyboard.event.GlobalKeyListener;
import lc.kra.system.mouse.GlobalMouseHook;
import lc.kra.system.mouse.event.GlobalMouseEvent;
import lc.kra.system.mouse.event.GlobalMouseListener;

import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

public class KeyListener {
	GlobalKeyboardHook keyboard_hook;
	GlobalMouseHook mouse_hook;
	
	Map<Integer, String> vk_name;
	{
		vk_name = new HashMap<>();
		for (var f : GlobalKeyEvent.class.getDeclaredFields()) {
			int modifiers = f.getModifiers();
			if (Modifier.isPublic(modifiers) &&
				Modifier.isStatic(modifiers) &&
				Modifier.isFinal(modifiers) &&
				f.getType() == int.class) {
				try {
					vk_name.put(f.getInt(null), f.getName());
				} catch (IllegalAccessException e) {
					throw new RuntimeException(e);
				}
			}
		}
	}
	
	public interface Hook extends GlobalKeyListener, GlobalMouseListener {
		@Override
		default void keyPressed(GlobalKeyEvent event) {}
		@Override
		default void keyReleased(GlobalKeyEvent event) {}
		
		@Override
		default void mousePressed(GlobalMouseEvent event) {}
		@Override
		default void mouseReleased(GlobalMouseEvent event) {}
		@Override
		default void mouseMoved(GlobalMouseEvent event) {}
		@Override
		default void mouseWheel(GlobalMouseEvent event) {}
	}
	KeyListener() {
		boolean raw = true;
		keyboard_hook = new GlobalKeyboardHook(raw);
		mouse_hook = new GlobalMouseHook(raw);
	}
	
	void stop() {
		synchronized (lock) {
			lock.notifyAll();
		}
	}
	
	void run(Hook hook) throws InterruptedException {
		keyboard_hook.addKeyListener(hook);
		mouse_hook.addMouseListener(hook);
		synchronized (lock) {
			lock.wait();
		}
		keyboard_hook.shutdownHook();
		mouse_hook.shutdownHook();
	}
	
	private static final Object lock = new Object();
	
	public static void main(String[] args) {
		KeyListener kl = new KeyListener();
		try {
			kl.run(new Hook() {
				@Override
				public void keyPressed(GlobalKeyEvent event) {
					if (event.getVirtualKeyCode() == GlobalKeyEvent.VK_L)
						kl.stop();
				}
			});
		} catch (InterruptedException ignored) {}
	}
}