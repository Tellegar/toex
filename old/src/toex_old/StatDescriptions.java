package com.tellegar.toex_old;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StatDescriptions implements Serializable {
	static final Pattern format_p = Pattern.compile("\\{.+?[}\\]]");
	//	static final Pattern number_p = Pattern.compile("[+-]*\\d+(\\.\\d+)?");
	static final Pattern number_p = Pattern.compile("[+-]*(?:\\d+(?:\\.\\d*)?|\\.\\d+)");
	static final Pattern hash_pattern = Pattern.compile("[+-]?" + "\\.?" + format_p + "\\.?" + "|" + number_p);
	
	List<Description> descriptions;
	Map<String, List<Description.Generator>> map = new HashMap<>();
	
	static final boolean english_only = false;
	
	public StatDescriptions(List<Description> descriptions) {
		this.descriptions = descriptions;
		for (var description : this.descriptions) {
			for (var generator : description.generators) {
				if (english_only && !generator.language.isEmpty()) continue;
				
				// fix for generator for `local_unique_jewel_projectile_damage_+1%_per_x_dex_in_radius`
				//   "範圍內每 {0:d} 點已配置敏捷，增加 {1}% 投射物傷害"
				// -> "範圍內每 {0:d} 點已配置敏捷，增加 1% 投射物傷害"
				String fs = generator.format_string;
				generator.format_string = format_p.matcher(fs).replaceAll(match_result -> {
					String format_pattern = match_result.group();
					FormatPattern fp = StatDescriptions.parse_format_pattern(format_pattern);
					if (fp.index < 0 || description.variables.length <= fp.index)
						return format_pattern.substring(1, format_pattern.length() - 1);
					return format_pattern;
				});
				
				String hash = hash_pattern.matcher(generator.format_string).replaceAll("{}");
				
				if (!map.containsKey(hash))
					map.put(hash, new ArrayList<>());
				map.get(hash).add(generator);
			}
		}
	}
	
	public StatDescriptions() throws IOException {
		this(
			Description.extract(
				Path.of(
					"export/ggpk/metadata/statdescriptions/stat_descriptions.txt"
				)
			)
		);
	}
	
	public static StatDescriptions extract() throws IOException {
		return new StatDescriptions(
			Description.extract(
				Path.of(
					"export/ggpk/metadata/statdescriptions/stat_descriptions.txt"
				)
			)
		);
	}
	
	public interface Scope {
		double get_value();
		
		default boolean is_satisfied(double value) {
			return false;
		}
		
		/**
		 * @return {@link Decimal} if needed else {@link Range}
		 */
		static Scope of(double value) {
			return value % 1 == 0 ? Range.of((int) value) : Decimal.of(value);
		}
	}
	
	public static class Range implements Scope {
		Integer low = null, high = null;
		
		@Override
		public double get_value() {
			assert low != null && low.equals(high);
			return low;
		}
		
		@Override
		public boolean is_satisfied(double value) {
			if (low != null && Math.round(value) < low) return false;
			if (high != null && high < Math.round(value)) return false;
			return true;
		}
		
		private Range() {}
		
		public static Range of(Integer low, Integer high) {
			Range range = new Range();
			range.low = low;
			range.high = high;
			return range;
		}
		
		public static Range of(int value) {
			return of(value, value);
		}
		
		
		@Override
		public String toString() {
			if (low == null && high == null) return "#";
			else if (low == null) return "#|" + high;
			else if (high == null) return low + "|#";
			else if (low.equals(high)) return low.toString();
			else return low + "|" + high;
		}
		
		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o instanceof Range range)
				return Objects.equals(low, range.low) && Objects.equals(high, range.high);
			if (o instanceof Decimal decimal)
				return Objects.equals(this, Range.of((int) Math.round(decimal.value)));
			return false;
		}
	}
	
	static class NotValue implements Scope {
		int value;
		
		@Override
		public double get_value() {
			assert false : "NotValue doesn't have value";
			return value;
		}
		
		@Override
		public boolean is_satisfied(double value) {
			return this.value != value;
		}
		
		NotValue() {}
		
		static NotValue of(int not_value) {
			NotValue nv = new NotValue();
			nv.value = not_value;
			return nv;
		}
		
		@Override
		public String toString() {
			return "!" + value;
		}
		
		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o instanceof NotValue not_value)
				return value == not_value.value;
			return false;
		}
	}
	
	static class Decimal implements Scope {
		double value;
		
		@Override
		public double get_value() {
			return value;
		}
		
		Decimal() {}
		
		static Decimal of(double value) {
			Decimal decimal = new Decimal();
			decimal.value = value;
//			decimal.value = Math.round(value * 10000) / 10000.;
			return decimal;
		}
		
		@Override
		public String toString() {
			return Double.toString(value);
//			return String.format("%.5f", value); // division can lose up to 3 decimal points (/1000)
		}
		
		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o instanceof Decimal decimal)
				return value == decimal.value;
			return false;
		}
	}
	
	public static Scope parse_variable_scope(String scope) {
		if (scope.startsWith("!"))
			return NotValue.of(Integer.parseInt(scope.substring(1)));
		
		// 1|1|# "Lacaios têm {0}% de Mana máxima aumentada"
		if ("1|1|#".equals(scope)) scope = scope.substring(2);
		
		String[] arr = scope.split("\\|");
		assert arr.length == 1 || arr.length == 2;
		
		Integer low = null, high = null;
		if (!"#".equals(arr[0])) low = Integer.parseInt(arr[0]);
		if (arr.length == 1) high = low;
		else if (!"#".equals(arr[1])) high = Integer.parseInt(arr[1]);
		
		return Range.of(low, high);
	}
	
	static class FormatPattern {
		int index = 0;
		String argument = null;
	}
	
	static FormatPattern parse_format_pattern(String pattern) {
		pattern = pattern.substring(1, pattern.length() - 1);
		String[] arr = pattern.split(":");
		assert arr.length <= 2;
		
		FormatPattern fp = new FormatPattern();
		
		if (!arr[0].isEmpty())
			fp.index = Integer.parseInt(arr[0]);
//		else
//			assert generator.variable_scopes.length == 1;
		
		if (arr.length == 2)
			fp.argument = arr[1];
		
		return fp;
	}
	
	/**
	 * Applies {@link Description.Generator#arguments} to non-null values.
	 *
	 * @param values replaces each modified value by new Scope, instead of rewriting
	 */
	public static void apply_arguments(Scope[] values, String[] arguments, boolean reverse_direction) {
		// TODO reverse_direction needs the arguments to be reordered
		//  [x+100, x*2]  ~  [x/2, x-100]
		for (int i = 0; i < arguments.length; i++) {
			String argument = arguments[i];
			switch (argument) {
				case "reminderstring" -> i++;
				case "canonical_line" -> {}
				case "canonical_stat" -> i++; // does this do something?
				default -> {
					Function<Object, Double> convert = obj -> {
						if (obj instanceof Integer val)
							return val.doubleValue();
						return (Double) obj;
					};
					Integer add = switch (argument) {
						case "multiplicative_damage_modifier" -> 100;
						case "plus_two_hundred" -> 200;
						default -> null;
					};
					Double multiply = convert.apply(switch (argument) {
						case "negate" -> -1;
						case "times_twenty" -> 20;
						case "times_one_point_five" -> 1.5;
						case "60%_of_value" -> .60;
						case "negate_and_double" -> -2;
						case "double" -> 2;
						default -> null;
					});
					Double divide = convert.apply(switch (argument) {
						case "divide_by_one_hundred" -> 100;
						case "divide_by_one_hundred_2dp" -> 100;
						case "divide_by_one_hundred_2dp_if_required" -> 100;
						case "divide_by_one_hundred_and_negate" -> -100;
						
						case "per_minute_to_per_second" -> 60;
						case "per_minute_to_per_second_0dp" -> 60;
						case "per_minute_to_per_second_1dp" -> 60;
						case "per_minute_to_per_second_2dp" -> 60;
						case "per_minute_to_per_second_2dp_if_required" -> 60;
						
						case "milliseconds_to_seconds" -> 1000;
						case "milliseconds_to_seconds_0dp" -> 1000;
						case "milliseconds_to_seconds_1dp" -> 1000;
						case "milliseconds_to_seconds_2dp" -> 1000;
						case "milliseconds_to_seconds_2dp_if_required" -> 1000;
						case "divide_by_one_thousand" -> 1000;
						
						case "deciseconds_to_seconds" -> 10;
						case "locations_to_metres" -> 10;
						case "divide_by_ten_0dp" -> 10;
						case "divide_by_ten_1dp" -> 10;
						case "divide_by_ten_1dp_if_required" -> 10;
						
						case "divide_by_twelve" -> 12;
						case "divide_by_six" -> 6;
						case "divide_by_fifteen_0dp" -> 15;
						case "divide_by_twenty_then_double_0dp" -> 10; // rounding ?
						case "divide_by_two_0dp" -> 2;
						case "divide_by_three" -> 3;
						case "divide_by_five" -> 5;
						case "divide_by_fifty" -> 50;
						case "divide_by_twenty" -> 20;
						case "divide_by_four" -> 4;
						default -> null;
					});
					
					int index = Integer.parseInt(arguments[++i]) - 1;
					if (values[index] == null) continue;
					
					double value = values[index].get_value();
					if (add != null)
						value = reverse_direction ? value - add : value + add;
					else if (multiply != null)
						value = reverse_direction ? value / multiply : value * multiply;
					else if (divide != null)
						value = reverse_direction ? value * divide : value / divide;
					else
						assert false : "`" + argument + "` is not handled";
					values[index] = Scope.of(value);
				}
			}
		}
	}
	
	static private Map<String, Scope> parse_modifier_using_generator(String modifier, Description.Generator generator) {
		Description description = generator.get_description();
		
		// could be extracted to Description/Generator
		String fs = generator.format_string;
		fs = Pattern.quote(fs);
		List<FormatPattern> format_patterns = new ArrayList<>();
		fs = format_p.matcher(fs).replaceAll(match_result -> {
			String pattern = match_result.group();
			FormatPattern fp = parse_format_pattern(pattern);
			format_patterns.add(fp);
			if (fp.argument != null && fp.argument.startsWith("+"))
//				return "\\\\E([+-]?\\\\d+(\\\\.\\\\d+)?)\\\\Q";
				return "\\\\E([+-]?(?:\\\\d+(?:\\\\.\\\\d*)?|\\\\.\\\\d+))\\\\Q";
//			return "\\\\E(-?\\\\d+(\\\\.\\\\d+)?)\\\\Q";
			return "\\\\E(-?(?:\\\\d+(?:\\\\.\\\\d*)?|\\\\.\\\\d+))\\\\Q";
		});
		// ---------------------------------
		
		Scope[] scopes = new Scope[description.variables.length];
		for (int i = 0; i < scopes.length; i++)
			scopes[i] = parse_variable_scope(generator.variable_scopes[i]);
		Scope[] values = new Scope[scopes.length];
//		Scope[] values = Arrays.copyOf(scopes, scopes.length);
		
		Matcher matcher = Pattern.compile(fs).matcher(modifier);
		if (matcher.matches())
			for (int group = 0; group < format_patterns.size(); group++) {
				FormatPattern fp = format_patterns.get(group);
				values[fp.index] = Scope.of(Double.parseDouble(matcher.group(group + 1)));
//				if (variables[fp.index] instanceof Range range) {
//					if (range.low != null && value < range.low) return null;
//					if (range.high != null && range.high < value) return null;
//				}
//				variables[fp.index] = Scope.of(value);
//				if (matcher.group(2 * group + 2) == null) { // integer
//					int value = Integer.parseInt(matcher.group(2 * group + 1));
//					variables[fp.index] = Range.of(value);
//				} else { // decimal
//					double value = Double.parseDouble(matcher.group(2 * group + 1));
//					variables[fp.index] = Decimal.of(value);
//				}
			}
		else return null;
		
		// fails if the arguments apply to non-used values (should not happen)
		apply_arguments(values, generator.arguments, true);
		
		for (int i = 0; i < description.variables.length; i++) {
			if (values[i] == null)
				values[i] = scopes[i];
			else if (!scopes[i].is_satisfied(values[i].get_value()))
				return null;
		}
		
		Map<String, Scope> out = new HashMap<>();
		for (int i = 0; i < scopes.length; i++)
			out.put(description.variables[i], values[i]);
		return out;
	}
	
	/**
	 * Parses the modifier and returns a list of mod-stats for each generator that could return this modifier.
	 *
	 * @return null if no relevant generator was found
	 */
	public List<Map<String, Scope>> match_modifier(String modifier) {
		List<Map<String, Scope>> out = new ArrayList<>();
		String hash = number_p.matcher(modifier).replaceAll("{}");
		if (!map.containsKey(hash)) return null;
		for (var generator : map.get(hash)) {
			Map<String, Scope> mod_stats = parse_modifier_using_generator(modifier, generator);
			if (mod_stats == null) continue;
			if (out.contains(mod_stats)) continue;
			out.add(mod_stats);
		}
		return out;
	}
}