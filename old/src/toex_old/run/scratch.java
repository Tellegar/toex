package com.tellegar.toex_old.run;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.tellegar.toex_old.DatTable;
import com.tellegar.toex_old.StatDescriptions;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.tellegar.toex_old.Extract.nop;

import com.tellegar.toex_old.StatDescriptions.Range;
import com.tellegar.toex_old.StatDescriptions.Scope;

interface Render {
	default String render() {
		return this.toString();
	}
}

interface Selection {
	void select(int selected);
	Object select();
	
	List<String> render_selection();
}

class ArraySelection<E> implements Selection {
	List<E> arr;
	int selected = 0;
	
	@SafeVarargs
	static <E> ArraySelection<E> of(E... elements) {
		ArraySelection<E> sel = new ArraySelection<>();
		sel.arr = List.of(elements);
		return sel;
	}
	static <E> ArraySelection<E> from(List<E> list) {
		ArraySelection<E> sel = new ArraySelection<>();
		sel.arr = list;
		return sel;
	}
	static <E> ArraySelection<E> from(
		JsonArray json_array,
		Function<JsonElement, E> constructor
	) {
		ArraySelection<E> sel = new ArraySelection<>();
		sel.arr = StreamSupport.stream(
				json_array.spliterator(), false
			)
			.map(constructor)
			.toList();
		return sel;
	}
	
	@Override
	public Object select() {
		return arr.get(selected);
	}
	@Override
	public void select(int selected) {
		this.selected = Math.clamp(selected, 0, arr.size() - 1);
	}
	
	@Override
	public List<String> render_selection() {
		ArrayList<String> lines = new ArrayList<>();
		for (int i = 0; i < arr.size(); i++) {
			StringBuilder line = new StringBuilder();
			if (i == selected)
				line.append("\033[7m");
			if (arr.get(i) instanceof Render r)
				line.append(r.render());
			else
				line.append(arr.get(i));
			if (i == selected)
				line.append("\033[27m");
			lines.add(line.toString());
		}
		return lines;
	}
}

record Mod(String mod, String type) {
	@Override
	public String toString() {
		if ("explicit".equals(type))
			return mod;
		return mod + " (" + type + ")";
	}
}

class Item implements Render {
	JsonObject item;
	Item(JsonElement item) {
		this.item = item.getAsJsonObject();
	}
	
	private String print_in_color(String str, int r, int g, int b) {
		return "\033[38;2;%d;%d;%dm".formatted(r, g, b) + str + "\033[0m";
	}
	
	@Override
	public String render() {
		String rarity = item.has("rarity") ? item.get("rarity").getAsString() : null;
		String name = item.get("name").getAsString();
		
		return switch (rarity) {
			case "Normal" -> print_in_color(name, 200, 200, 200);
			case "Magic" -> print_in_color(name, 136, 136, 255);
			case "Rare" -> print_in_color(name, 255, 255, 119);
			case "Unique" -> print_in_color(name, 175, 96, 37);
			case null -> name;
			default -> throw new RuntimeException("rarity: " + rarity + " is not handled");
		};
	}
	
	List<Mod> get_mods() {
		List<Mod> mods = new ArrayList<>();
		for (String mod_type : List.of(
			"implicitMods",
			"explicitMods",
			"craftedMods",
			"enchantMods",
			"scourgeMods",
			"ultimatumMods",
			"crucibleMods"
		)) {
			if (!item.has(mod_type)) continue;
			for (var modifier : item.get(mod_type).getAsJsonArray())
				mods.add(new Mod(
					modifier.getAsString(),
					mod_type.substring(0, mod_type.length() - 4)
				));
		}
		return mods;
	}
}

public class scratch {
	
	static void interface_scratch() {
//		JsonObject character_json = Utilities.loadJSON("character.json").getAsJsonObject()
//			.get("character").getAsJsonObject();
//
//		JsonArray equipment = character_json.get("equipment").getAsJsonArray();
////		ArraySelection<Item> items = new ArraySelection<>();
////		items.arr = new Item[equipment.size()];
////		for (int i = 0; i < equipment.size(); i++)
////			items.arr[i] = new Item(equipment.get(i));
//
//		ArraySelection<Item> items = ArraySelection.from(equipment, Item::new);
//		for (var item : items.render_selection())
//			System.out.println(item);
//		System.out.println();
//
//		Item item = (Item) items.select();
//
//		ArraySelection<Mod> mods = ArraySelection.from(item.get_mods());
//		mods.select(5);
//		for (var mod : mods.render_selection())
//			System.out.println(mod);
//		System.out.println();
//
//		Mod mod = (Mod) mods.select();
		Mod mod = new Mod("+24 to Strength and Intelligence", "crafted");

//		StatDescriptions stat_descriptions = (StatDescriptions) Utilities.load_from_file(
//			"src/main/resources/ggpk-export/stat_descriptions.ser"
//		);
//		var mod_stats = stat_descriptions.match_modifier(mod.mod());
		var mod_stats = List.of(
			Map.of("base_strength_and_intelligence", Scope.of(24)),
			Map.of("additional_strength_and_intelligence", Scope.of(24))
		);
		for (var mod_stat : ArraySelection.from(mod_stats).render_selection())
			System.out.println(mod_stat);
	}
	
	static StatDescriptions stat_descriptions;
	static DatTable mods;
	static DatTable stats;
	static DatTable mod_type;
	
	static {
		try {
			stat_descriptions = StatDescriptions.extract();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		try {
			mods = DatTable.from_name("Mods");
			stats = DatTable.from_name("Stats");
			mod_type = DatTable.from_name("ModType");
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
	
	static void modifier_match_scratch() throws IOException {

//		JsonArray columns = mods.schema.get("columns").getAsJsonArray();
//		for (int i = 0; i < columns.size(); i++) {
//			JsonObject column = columns.get(i).getAsJsonObject();
//			String name = column.get("name").isJsonNull() ? "?" : column.get("name").getAsString();
//			if (name.startsWith("StatsKey"))
//				System.out.println(i + " " + name);
//		}

//		int[] cols = new int[]{4, 5, 6, 7, 30, 39};
//		Key[] stats_keys = new Key[cols.length];
//		for (int i = 0; i < cols.length; i++) {
//			stats_keys[i] = (Key) mods.get(cols[i], 0);
//		}
		
		Map<String, Integer> stats_row_by_id = new HashMap<>();
		for (int row = 0; row < stats.row_count; row++)
			stats_row_by_id.put((String) stats.get(0, row), row);
		
		
		Map<Set<Integer>, List<Integer>> mod_rows_by_stat_set = new HashMap<>();
		for (int row = 0; row < mods.row_count; row++) {
			int[] cols = new int[]{4, 5, 6, 7, 30, 39};
			Set<Integer> keys = new HashSet<>();
			for (int col : cols) {
				DatTable.Key key = (DatTable.Key) mods.get(col, row);
				if (key.value != null)
					keys.add(key.value);
			}
			if (!mod_rows_by_stat_set.containsKey(keys))
				mod_rows_by_stat_set.put(keys, new ArrayList<>());
			mod_rows_by_stat_set.get(keys).add(row);
		}
		
		
		Map<String, List<Integer>> mod_row_by_name = new HashMap<>();
		for (int row = 0; row < mods.row_count; row++) {
			String name = (String) mods.get(9, row);
			if (!mod_row_by_name.containsKey(name))
				mod_row_by_name.put(name, new ArrayList<>());
			mod_row_by_name.get(name).add(row);
		}


//		StatDescriptions stat_descriptions = (StatDescriptions) Utilities.load_from_file("src/main/resources/ggpk-export/stat_descriptions.ser");
//		StatDescriptions stat_descriptions = StatDescriptions.extract();
		String mod = "+24 to Strength and Intelligence";
		
		var mod_stats = stat_descriptions.match_modifier(mod);
		for (var mod_stat : mod_stats) {
			var mod_stat_ = mod_stat.entrySet().stream().collect(Collectors.toMap(
				e -> stats_row_by_id.get(e.getKey()), Map.Entry::getValue
			));
			System.out.println(mod_stat_);
//			for (var e : mod_stat.entrySet()) {
//				System.out.println(e.getKey() + ": " + e.getValue());
//				System.out.println(stats_row_by_id.get(e.getKey()));
//			}
			List<Integer> mod_rows = mod_rows_by_stat_set.get(mod_stat_.keySet());
			System.out.println(mod_rows);
			List<Integer> filtered = mod_rows.stream().filter(mod_row -> {
				DatTable.Key domain = (DatTable.Key) mods.get("Domain", mod_row);
				DatTable.Key generation_type = (DatTable.Key) mods.get("GenerationType", mod_row);
				var constrains = get_mod_constrains(mods, mod_row);
				for (var entry : mod_stat_.entrySet()) {
					int key = entry.getKey();
					Scope value = entry.getValue();
					if (!constrains.get(key).is_satisfied(value.get_value()))
						return false;
				}
				System.out.println(constrains);
				System.out.println(domain + " " + generation_type);
				return true;
			}).toList();
			System.out.println(filtered);
			System.out.println();
			nop();
		}
		
		nop();
	}
	
	static Map<Integer, Range> get_mod_constrains(DatTable mods, int row) {
		Map<Integer, Range> ranges = new HashMap<>();
		for (int i = 1; i <= 6; i++) {
			DatTable.Key key = (DatTable.Key) mods.get("StatsKey" + i, row);
			if (key.value != null) {
				Integer low = (Integer) mods.get("Stat" + i + "Min", row);
				Integer high = (Integer) mods.get("Stat" + i + "Max", row);
				ranges.put(key.value, Range.of(low, high));
			}
		}
		return ranges;
	}
	
	public static void main(String[] args) throws IOException {
		modifier_match_scratch();
	}
}
