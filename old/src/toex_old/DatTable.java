package com.tellegar.toex_old;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

import static com.tellegar.toex_old.Extract.nop;

/**
 * represents content.ggpk dat file
 * <br>
 * using this <a href="https://github.com/poe-tool-dev/dat-schema">schema</a> for columns
 * <br>
 * note: <a href="https://youtu.be/XNN0RLIfg-Y?t=2689">developer's internal tool</a>}
 */
public class DatTable {
	public JsonObject schema = null;
	//	public Spec[] spec;
	ByteBuffer raw;
	int raw_size;
	Map<String, Integer> col_by_name;
	
	public int row_count;
	int row_size;
	int specified_size;
	int data_offset;
	
	int[] col_start;
	
	static JsonObject schemas = null;
	static Map<String, JsonObject> schema_by_name;
	static void load_schemas() throws FileNotFoundException {
		if (schemas != null) return;
		schemas = Utilities.loadJSON("src/main/resources/schema.min.json").getAsJsonObject();
		schema_by_name = new HashMap<>();
		JsonArray schemas_tables = schemas.get("tables").getAsJsonArray();
		for (int i = 0; i < schemas_tables.size(); i++) {
			JsonObject schema = schemas_tables.get(i).getAsJsonObject();
			schema_by_name.put(schema.get("name").getAsString().toLowerCase(), schemas_tables.get(i).getAsJsonObject());
		}
	}
	
	Map<String, Integer> type_size = Map.of(
		"bool", 1,
		"string", 8,
		"i32", 4,
		"f32", 4,
		"row", 8,
		"foreignrow", 16,
		"enumrow", 4
	);
	int get_type_size(int col) {
		JsonArray schema_columns = schema.get("columns").getAsJsonArray();
		JsonObject schema = schema_columns.get(col).getAsJsonObject();
		return get_type_size(schema);
	}
	int get_type_size(JsonObject schema) {
		return schema.get("array").getAsBoolean() ? 16 : type_size.get(schema.get("type").getAsString());
	}
	
	// https://snosme.github.io/poe-dat-viewer/
	public static DatTable from_name(String name) throws FileNotFoundException {
		String filename = name;
		if (name.contains("."))
			name = name.substring(0, name.indexOf(".")).toLowerCase();
		else
			filename += ".dat*";
		
		load_schemas();
		JsonObject schema = schema_by_name.get(name.toLowerCase());
		
		List<Path> matched_files = new ArrayList<>();
		try (DirectoryStream<Path> dirStream = Files.newDirectoryStream(
			Path.of("export/ggpk/data/"), filename
		)) {
			for (Path p : dirStream)
				matched_files.add(p);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

//		for (Path p : matched_files)
//			System.out.println(p);
		
		Optional<Path> dat64 = matched_files.stream().filter(p -> p.toString().endsWith(".dat64")).findAny();
		DatTable dt;
		if (dat64.isPresent())
			dt = new DatTable(dat64.get(), schema);
		else if (!matched_files.isEmpty())
			dt = new DatTable(matched_files.getFirst(), schema);
		else
			throw new RuntimeException(name + " not found");
		return dt;
	}
	public DatTable(Path path, JsonObject schema) throws FileNotFoundException {
		String filename = path.getFileName().toString();
		String extension = filename.split("\\.")[1];
		assert extension.equals("dat64") : "currently only supporting dat64"; // datl64 has utf-32 encoding
		
		this.schema = schema;
		
		try (RandomAccessFile file = new RandomAccessFile(path.toFile(), "r")) {
			FileChannel channel = file.getChannel();
			raw_size = (int) channel.size();
			raw = ByteBuffer.allocate(raw_size);
			raw.order(ByteOrder.LITTLE_ENDIAN);
			channel.read(raw);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		final int b = 4; // first 4 bytes are row_count
		row_count = raw.getInt(0);
		data_offset = -1; // self.raw.find(b"\xBB" * 8)
		for (int i = 0, j = 0; i < raw_size; i++) { // simple kmp
			if (raw.get(i) == (byte) 0xBB)
				j++;
			else
				j = 0;
			if (j == 8)
				data_offset = i - j + 1;
		}
		row_size = (data_offset - b) / row_count;
		
		// apply schema
		col_start = new int[this.schema.get("columns").getAsJsonArray().size()];
		col_start[0] = 4;
		specified_size = 0;
		col_by_name = new HashMap<>();
		JsonArray schema_columns = this.schema.get("columns").getAsJsonArray();
		for (int col = 0; col < schema_columns.size() - 1; col++) {
			JsonObject schema_column = schema_columns.get(col).getAsJsonObject();
			int size = get_type_size(schema_column);
			col_start[col + 1] = col_start[col] + size;
			specified_size += size;
			if (!schema_column.get("name").isJsonNull())
				col_by_name.put(schema_column.get("name").getAsString(), col);
		}
		specified_size += get_type_size(schema_columns.size() - 1);
		assert specified_size == row_size : "schema columns doesn't specify whole row";
	}
	
	public Object get(String col_name, int row) {
		return get(col_by_name.get(col_name), row);
	}
	public Object get(int col, int row) {
//		Spec spec = this.spec[col];
		JsonObject schema = this.schema.get("columns").getAsJsonArray().get(col).getAsJsonObject();
		int start = row * row_size + col_start[col];
		
		if (schema.get("array").getAsBoolean()) {
			long count = raw.getLong(start);
			long points_to = raw.getLong(start + 8);
			assert count < 1000 : "something may went wrong";
			
			int e_start = (int) points_to + data_offset;
			int size = type_size.get(schema.get("type").getAsString());
			
			Object[] out = new Object[(int) count];
			for (int i = 0; i < count; i++) {
				out[i] = get_value(e_start, schema);
				e_start += size;
			}
			return out;
		} else
			return get_value(start, schema);
	}
	
	Object get_value(int start, JsonObject schema) {
		String type = schema.get("type").getAsString();
		switch (type) {
			case "bool" -> {
				return raw.get(start) != 0;
			}
			case "string" -> {
				var out = new StringBuilder();
				for (int i = raw.getInt(start) + data_offset; ; i += 2) {
					char c = raw.getChar(i);
					if (c == 0) break;
					out.append(c);
				}
				return out.toString();
			}
			case "i32" -> {
				return raw.getInt(start);
			}
			case "f32" -> {
				return raw.getFloat(start);
			}
			case "row", "foreignrow", "enumrow" -> {
				boolean is_null = true;
				for (int i = 0; i < get_type_size(schema); i++)
					if (raw.get(start + i) != (byte) 0xfe) {
						is_null = false;
						break;
					}
				Integer value = null;
				if (!is_null)
					value = raw.getInt(start); // doesnt read all 16 bytes for foreignrow
				return new Key(value, schema.get("references"));
			}
		}
		assert false : "unhandled type: " + type;
		return null;
	}
	
	public static class Key {
		public Integer value;
		public JsonObject references;
		public Key(Integer value, String ref_to) {}
		public Key(Integer value, JsonElement references) {
			this.value = value;
			this.references = references.isJsonNull()? null : references.getAsJsonObject();
		}
		@Override
		public String toString() {
			return "<" + value + ">";
		}
	}
	
	public static void main(String[] args) throws FileNotFoundException {
//		DatTable dat = new DatTable("armourtypes.dat64");
//		new DatTable("C:\\Users\\talaris\\IdeaProjects\\Toex\\export\\ggpk\\data\\armourtypes.dat64");
//		new DatTable("export/ggpk/data/armourtypes.dat64");
//		new DatTable("armourtypes.dat64");
//		new DatTable("armourtypes");
//		new DatTable("armourTypes");

//		DatTable.from_name("armourtypes.dat64");
//		DatTable dat = DatTable.from_name("armourTypes");
//		DatTable dat = DatTable.from_name("baseitemtypes");
		DatTable dat = DatTable.from_name("BuffTemplates");
		System.out.println(dat.schema.get("name").getAsString());
		
		for (int i = 0; i < dat.schema.get("columns").getAsJsonArray().size(); i++) {
			JsonObject schema = dat.schema.get("columns").getAsJsonArray().get(i).getAsJsonObject();
			String name = schema.get("name").isJsonNull()? "?" : schema.get("name").getAsString();
			System.out.print(name);
			System.out.print(" ".repeat(30 - name.length()));
			String type = schema.get("type").getAsString();
			if (schema.get("array").getAsBoolean())
				type += "[]";
			System.out.print(type);
			System.out.print(" ".repeat(20 - type.length()));
			
			Object value = dat.get(i, 335);
			
			if (value.getClass().isArray())
				System.out.println(Arrays.toString((Object[]) value));
			else
				System.out.println(value);
		}
		nop();
	}
}
