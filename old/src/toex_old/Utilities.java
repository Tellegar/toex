package com.tellegar.toex_old;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.*;

public class Utilities {
	public static void saveJSON(String path, JsonElement json) {
		try (FileWriter file = new FileWriter(path)) {
			file.write(json.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static JsonElement loadJSON(String path) throws FileNotFoundException {
		return JsonParser.parseReader(new FileReader(path));
	}
	
	public static <T> void save_to_file(T obj, String path) {
		try (
			var fos = new FileOutputStream(path);
			var oos = new ObjectOutputStream(fos)
		) {
			oos.writeObject(obj);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	public static Object load_from_file(String path) {
		try (
			var fis = new FileInputStream(path);
			var ois = new ObjectInputStream(fis)
		) {
			return ois.readObject();
		} catch (IOException | ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
}
