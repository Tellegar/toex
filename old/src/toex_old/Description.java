package com.tellegar.toex_old;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * represents one description from metadata/statdescriptions/* files
 */
public class Description implements Serializable {
	private Description() {
		language = "";
	}
	
	public String[] variables;
	public List<Generator> generators = null;
	
	public class Generator implements Serializable {
		public String language;
		public String[] variable_scopes;
		// i - any
		// 1|# - at least 1
		// #|0 - at most 0
		public String format_string;
		// format {0}: 0 - index of variable
		public String[] arguments;
		// - negate 1: negate first variable (index 0)
		// - reminderstring <>: in game visualisation for specifying something used in the description
		//   eg: "Gain Onslaught for {0} second per Frenzy Charge on use" reminderstring ReminderTextOnslaught
		//   in game with alt: Gain Onslaught for 3 seconds per Frenzy Charge on use
		//                (Onslaught grants 20% increased Attack, Cast, and Movement Speed)
		// - per_minute_to_per_second 1: divide by 60
		
		public Description get_description() {
			return Description.this;
		}
		
		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o instanceof Generator generator) {
				if (!Objects.equals(language, generator.language)) return false;
				if (!Arrays.equals(variable_scopes, generator.variable_scopes)) return false;
				if (!Objects.equals(format_string, generator.format_string)) return false;
				return Arrays.equals(arguments, generator.arguments);
			}
			return false;
		}
		
		/**
		 * @return true when not supported
		 */
		private static boolean check_support(List<String> arguments) {
			if (arguments.contains("display_indexable_support"))
				return true;
			if (arguments.contains("display_indexable_skill"))
				return true;
			if (arguments.contains("mod_value_to_item_class"))
				return true;
			if (arguments.contains("tree_expansion_jewel_passive"))
				return true;
			if (arguments.contains("passive_hash"))
				return true;
			if (arguments.contains("affliction_reward_type"))
				return true;
			if (arguments.contains("weapon_tree_unique_base_type_name"))
				return true;
			return false;
		}
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o instanceof Description description) {
			if (!Arrays.equals(variables, description.variables)) return false;
			return Objects.equals(generators, description.generators);
		}
		return false;
	}
	
	static List<String> non_consumed_lines;
	public static List<Description> extract(Path path) throws IOException {
		non_consumed_lines = new ArrayList<>();
		List<Description> descriptions = new ArrayList<>();
		try (BufferedReader r = Files.newBufferedReader(path, StandardCharsets.UTF_16LE)) {
			//noinspection ResultOfMethodCallIgnored
			r.read(); // eat '\xfeff'
			enum State {
				description_or_language, variables, count, generators
			}
			State state = State.description_or_language;
			int count = 0;
			while (true) {
				String line = r.readLine();
				if (line == null) break;
				line = line.strip();
				
				if (line.isEmpty()) continue;
				
				switch (state) {
					case description_or_language -> {
						if ("description".equals(line)) {
							state = State.variables;
							// out.append({})
							if (!descriptions.isEmpty() && descriptions.getLast().check_support())
								descriptions.removeLast();
							descriptions.add(new Description());
						} else if (line.startsWith("lang ")) {
							descriptions.getLast().setLanguage(line);
							state = State.count;
						} else
							non_consumed_lines.add(line);
					}
					case variables -> {
						String[] vals = line.split(" ");
						count = Integer.parseInt(vals[0]);
						String[] variables = Arrays.copyOfRange(vals, 1, vals.length);
						assert count == variables.length : "count and variables.length don't match";
						//out[-1]["variables"] = variables
						descriptions.getLast().variables = variables;
						state = State.count;
					}
					case count -> {
						count = Integer.parseInt(line);
						state = State.generators;
						//out[-1]["generators"] = out[-1].get("generators", [])
						if (descriptions.getLast().generators == null)
							descriptions.getLast().generators = new ArrayList<>();
					}
					case generators -> {
						if (count > 0) {
							count--;
							//out[-1]["generators"].append((lang, line.lstrip().split(" ", len(out[-1]["variables"]))))
							descriptions.getLast().addGenerator(line);
						}
						if (count == 0) state = State.description_or_language;
					}
				}
			}
		}
		return descriptions;
	}
	
	/**
	 * @return true when not supported
	 */
	private boolean check_support() {
		if (Arrays.stream(variables).anyMatch(v -> v.startsWith("old_do_not_use_")))
			return true;
		return false;
	}
	
	private static String language;
	
	private void setLanguage(String line) {
		language = line.replace("lang ", "");
		language = language.substring(1, language.length() - 1);
	}
	
	private void addGenerator(String line) {
		var gen = new Generator();
		
		//gen.line = line;
		gen.language = language;
		
		String[] vars = line.split("\\s+", variables.length + 1);
		assert vars[vars.length - 1].charAt(0) == '"' : "format_string doesn't begin with \", failed to split variable_scopes";
		gen.variable_scopes = Arrays.copyOf(vars, variables.length);
		line = vars[vars.length - 1];
		
		assert line.chars().filter(c -> c == '"').count() == 2 : "multiple \" are not handled";
		gen.format_string = line.substring(1, line.indexOf('"', 1));
		gen.format_string = gen.format_string.replaceAll("\\\\n", "\n");
		
		line = line.substring(line.indexOf('"', 1) + 1).strip();
		if (line.isEmpty()) gen.arguments = new String[0];
		else gen.arguments = line.split("\\s+");
		
		if (!Generator.check_support(Arrays.asList(gen.arguments)))
			generators.add(gen);
	}
}
