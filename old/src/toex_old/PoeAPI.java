package com.tellegar.toex_old;

import com.nimbusds.oauth2.sdk.*;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.State;
import com.nimbusds.oauth2.sdk.pkce.CodeChallengeMethod;
import com.nimbusds.oauth2.sdk.pkce.CodeVerifier;
import com.nimbusds.oauth2.sdk.token.AccessToken;
import com.nimbusds.oauth2.sdk.token.BearerTokenError;

import com.nimbusds.oauth2.sdk.token.RefreshToken;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;

import com.google.gson.*;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.http.*;
import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.prefs.Preferences;
import java.util.stream.Collectors;

public class PoeAPI {
	private static final Preferences pref = Preferences.userRoot().node("toex");
	
	final String endpoint = "https://api.pathofexile.com";
	String auth_header = null;
	
	public PoeAPI() {
		auth_header = "Bearer " + pref.get("access_token", null);
//		Duration d = Duration.between(Instant.now(), Instant.ofEpochSecond(pref.getLong("access_token_expiration", 0)));
//		System.out.printf("expires in: %d:%02d:%02d\n", d.toHours(), d.toMinutesPart(), d.toSecondsPart());
	}
	
	public Duration auth_expiry() {
		if (auth_header == null) return Duration.ZERO;
		return Duration.between(
			Instant.now(),
			Instant.ofEpochSecond(pref.getLong(
				"access_token_expiration",
				0
			))
		);
	}
	
	/**
	 * Authorizes the application using the OAuth protocol.
	 * This method will launch the OAuth flow and obtain the access token for the API.
	 * It is required to call this method before making any API requests.
	 * <p>
	 * This method is blocking.
	 */
	public void authorize() {
		System.out.println(OAuth.A());
		OAuth.authRedirectRequestURI = null;
		OAuth.B();
		while (OAuth.authRedirectRequestURI == null) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
		}
		OAuth.server.stop(0);
		OAuth.C(OAuth.authRedirectRequestURI, OAuth.state);
		
		try {
			OAuth.D();
		} catch (IOException | ParseException e) {
			throw new RuntimeException(e);
		}
		
		System.out.println("access_token: " + OAuth.accessToken);
		pref.put("access_token", String.valueOf(OAuth.accessToken));
		pref.putLong("access_token_expiration", Instant.now().getEpochSecond() + OAuth.expires_in);
		auth_header = "Bearer " + OAuth.accessToken;
	}
	
	private final HttpClient client = HttpClient.newHttpClient();
	
	public JsonElement request(String resource) throws InterruptedException {
		return request(resource, null);
	}
	public JsonElement request(String resource, Map<String, String> parameters) throws InterruptedException {
		String qp = "";
		if (parameters != null)
			qp = parameters.entrySet().stream()
				.map(entry -> entry.getKey() + "=" + entry.getValue())
				.collect(Collectors.joining("&"));
		if (!qp.isEmpty()) qp = "?" + qp;
		
		HttpRequest request = HttpRequest.newBuilder()
			.GET()
			.uri(URI.create(endpoint + resource + qp))
			.header("Authorization", auth_header)
			.build();
		
		HttpResponse<String> response;
		try {
			response = client.send(request, HttpResponse.BodyHandlers.ofString());
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());
		}
		
		String wwwAuthHeader = response.headers().firstValue("www-authenticate").orElse(null);
		if (wwwAuthHeader != null) {
			System.out.println(wwwAuthHeader);
			try {
				var error = BearerTokenError.parse(wwwAuthHeader);
				if (error.equals(BearerTokenError.MISSING_TOKEN)) {
					// Handle missing token...
				} else if (error.equals(BearerTokenError.INVALID_REQUEST)) {
					// Malformed request...
				} else if (error.equals(BearerTokenError.INVALID_TOKEN)) {
					// Refresh / obtain new token...
					// TODO
					System.out.println("request new token");
				} else if (error.equals(BearerTokenError.INSUFFICIENT_SCOPE)) {
					// Overstepped authorisation / obtain new token with required scope
				}
			} catch (ParseException e) {
				System.err.println(e.getMessage());
			}
		}
		
		if (!response.body().isEmpty())
			return JsonParser.parseString(response.body());
		
		JsonElement e = JsonParser.parseString(response.body());
		
		int i = 0;
		throw new RuntimeException("no response body");
	}
}

class OAuth {
	static CodeVerifier pkceVerifier;
	static State state;
	static URI redirectURI;
	public static URI A() {
		pkceVerifier = new CodeVerifier();
		state = new State();
		
		URI endpoint = URI.create("https://www.pathofexile.com/oauth/authorize");
		redirectURI = URI.create("http://localhost/");
		Scope scope = new Scope("account:stashes", "account:characters", "account:leagues", "account:league_accounts");
		
		AuthorizationRequest request = new AuthorizationRequest.Builder(
			new ResponseType("code"),
			new ClientID("toex"))
			.endpointURI(endpoint)
			.redirectionURI(redirectURI)
			.scope(scope)
			.codeChallenge(pkceVerifier, CodeChallengeMethod.S256)
			.state(state)
			.build();
		
		return request.toURI();
	}
	
	static HttpServer server;
	static URI authRedirectRequestURI;
	// This is a helper method
	public static void handle(HttpExchange exchange) throws IOException {
		URI uri = exchange.getRequestURI();
		if ("/favicon.ico".equals(uri.toString())) return;
		authRedirectRequestURI = uri;
		// TODO check the URI here
		// Handle your authorization and token exchange here
		String response = "Success";
		exchange.sendResponseHeaders(200, response.getBytes().length);
		OutputStream os = exchange.getResponseBody();
		os.write(response.getBytes());
		os.close();
	}
	static void B() {
		try {
			server = HttpServer.create(new InetSocketAddress(80), 0);
			HttpContext context = server.createContext("/", OAuth::handle);
			server.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	static AuthorizationCode authCode;
	static void C(URI uri, State state) {
		// Parse the authorisation response from the callback URI
		AuthorizationResponse response;
		try {
			response = AuthorizationResponse.parse(uri);
		} catch (ParseException x) {
			System.err.println(uri);
			throw new IllegalArgumentException(x.getMessage(), x);
		}
		
		// Check the returned state parameter, must match the original
		if (!state.equals(response.getState())) {
			// Unexpected or tampered response, stop!!!
			return;
		}
		
		if (!response.indicatesSuccess()) {
			// The request was denied or some error occurred
			AuthorizationErrorResponse errorResponse = response.toErrorResponse();
			System.out.println(errorResponse.getErrorObject());
			return;
		}
		
		AuthorizationSuccessResponse successResponse = response.toSuccessResponse();
		
		// Retrieve the authorisation code, to be used later to exchange the code for
		// an access token at the token endpoint of the server
		authCode = successResponse.getAuthorizationCode();
	}
	
	static AccessToken accessToken;
	static RefreshToken refreshToken;
	static long expires_in; // access token expiry
	static void D() throws IOException, ParseException {
		// Make the token request, with PKCE
		TokenRequest tokenRequest = new TokenRequest(
			URI.create("https://www.pathofexile.com/oauth/token"),
			new ClientID("toex"),
			new AuthorizationCodeGrant(authCode, redirectURI, pkceVerifier));
		
		var a = tokenRequest.toHTTPRequest().send();
		TokenResponse response = TokenResponse.parse(a);
		
		if (!response.indicatesSuccess()) {
			// We got an error response...
			TokenErrorResponse errorResponse = response.toErrorResponse();
		}
		
		AccessTokenResponse successResponse = response.toSuccessResponse();
		
		// Get the access token, the server may also return a refresh token
		accessToken = successResponse.getTokens().getAccessToken();
		refreshToken = successResponse.getTokens().getRefreshToken();
		expires_in = successResponse.toJSONObject().getAsNumber("expires_in").longValue();
	}
}