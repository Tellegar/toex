package com.tellegar.toex_old.a;

import com.sun.jna.*;
import com.sun.jna.platform.win32.WinDef.HWND;
import com.sun.jna.win32.StdCallLibrary;
import com.sun.jna.win32.W32APIOptions;

import java.util.Arrays;
import java.util.List;

public interface User32 extends StdCallLibrary {
	User32 INSTANCE = (User32) Native.load("user32", User32.class, W32APIOptions.DEFAULT_OPTIONS);
	
	HWND FindWindow(String lpClassName, String lpWindowName);
	boolean GetClientRect(HWND hWnd, RECT rect);
	boolean ClientToScreen(HWND hWnd, POINT point);
	int SetWindowLong(HWND hWnd, int nIndex, int dwNewLong);
	
//	class HWND extends PointerType {
//		public HWND() {
//			super();
//		}
//		public HWND(Pointer address) {
//			super(address);
//		}
//	}
	
	class RECT extends Structure {
		public int x, y, width, height;
		
		public RECT() {
			x = 0;
			y = 0;
			width = 0;
			height = 0;
		}
		public RECT(int x, int y, int width, int height) {
			this.x = 0;
			this.y = 0;
			this.width = 0;
			this.height = 0;
		}
		
		@Override
		protected List<String> getFieldOrder() {
			return Arrays.asList("x", "y", "width", "height");
		}
	}
	
	class POINT extends Structure {
		public int x, y;
		
		public POINT() {
			x = 0;
			y = 0;
		}
		public POINT(int x, int y) {
			this.x = x;
			this.y = y;
		}
		
		@Override
		protected List<String> getFieldOrder() {
			return Arrays.asList("x", "y");
		}
	}
}
