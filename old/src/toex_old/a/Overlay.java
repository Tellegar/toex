package com.tellegar.toex_old.a;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.*;

import com.tellegar.toex_old.a.User32.*;

import static com.sun.jna.platform.win32.WinDef.HWND;

public class Overlay {
	
	static RECT f() {
		HWND poe_handle = User32.INSTANCE.FindWindow(null, "Path of Exile");
		if (poe_handle == null) return null;
		
		RECT r = new RECT();
		boolean r1 = User32.INSTANCE.GetClientRect(poe_handle, r);
		POINT p = new POINT();
		boolean r2 = User32.INSTANCE.ClientToScreen(poe_handle, p);
		r.x = p.x;
		r.y = p.y;
		
		return r;
	}
	
	public static void main(String[] args) {
		System.setProperty("sun.java2d.uiScale", "1.0");
		
		JFrame frame = new JFrame("Toex");
		
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice gd = ge.getDefaultScreenDevice();
		if (!gd.isWindowTranslucencySupported(GraphicsDevice.WindowTranslucency.PERPIXEL_TRANSLUCENT)) {
			System.out.println("Per-pixel translucency is not supported.");
			return;
		}
		
		int w = 300, h = 200;
		
		frame.setAlwaysOnTop(true);
		frame.setUndecorated(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBackground(new Color(0, 0, 0, 0));
		
		frame.setLocation(100, 100);
		frame.setSize(w, h);
		
		RECT r = f();
		
		if (r == null) {
			System.out.println("couldn't find poe window");
			return;
		} else {
			System.out.printf("%d, %d, %d, %d\n", r.x, r.y, r.width, r.height);
		}
		int x = 0;
		
		frame.setLocation(r.x, r.y);
		frame.setSize(r.width, r.height);
		
		// click-through
//		try {
//			Field field = Component.class.getDeclaredField("peer");
////			field.setAccessible(true);
//			Object peer = field.get(frame);
//			nop();
//		} catch (Exception e) {
//			throw new RuntimeException(e);
//		}
//		User32.INSTANCE.SetWindowLong(
//			((WFramePeer) frame.peer).hwnd,
//			GWL_EXSTYLE,
//			WS_EX_LAYERED | WS_EX_TRANSPARENT
//		);
		
		JPanel panel = new JPanel() {
			@Override
			public void paintComponent(Graphics g) {
				Graphics2D g2d = (Graphics2D) g;
				
				g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_SPEED);
//				g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				
				g2d.setColor(Color.RED);
				g2d.clearRect(0, 0, r.width, r.height);
				g2d.drawRect(0, 0, r.width - 1, r.height - 1);
				
				g2d.setColor(new Color(0x80, 0x80, 0x80, 0x80));
				g2d.fillRect(10, 10, 20, 30);

//				g2d.setColor(Color.BLACK);
//				g2d.drawRect(50, 50, 100, 100);
			}
		};
		frame.add(panel);
		frame.setVisible(true);
		Graphics g = frame.getGraphics();
		g.setColor(Color.RED);
		g.clearRect(0, 0, r.width, r.height);
		g.drawRect(0, 0, r.width - 1, r.height - 1);
		
		g.setColor(new Color(0x80, 0x80, 0x80, 0x80));
		g.fillRect(10, 10, 20, 30);

//		System.out.println("here");
//
//		KeyListener.main(args);
	}
}