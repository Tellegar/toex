package com.tellegar.toex_old;

import com.google.gson.*;

import com.diogonunes.jcolor.Attribute;
import com.google.gson.stream.JsonWriter;

import java.io.FileNotFoundException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.*;

import static com.diogonunes.jcolor.Ansi.colorize;
import static com.tellegar.toex_old.Extract.nop;

public class Build {
	static StatDescriptions stat_descriptions;
	static JsonObject skill_tree, atlas_tree;
	
	static {
		long start = System.nanoTime();
		System.out.print("loading static Build data...");
		stat_descriptions = (StatDescriptions) Utilities.load_from_file("src/main/resources/ggpk-export/stat_descriptions.ser");
		try {
			skill_tree = Utilities.loadJSON("src/main/resources/skilltree-export/data.json").getAsJsonObject();
			atlas_tree = Utilities.loadJSON("src/main/resources/atlastree-export/data.json").getAsJsonObject();
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
		long end = System.nanoTime();
		System.out.println(" " + (end - start) / 1_000_000_000. + "s done");
	}
	
	private String prettyJson(JsonElement element) {
		var string_writer = new StringWriter();
		var writer = new JsonWriter(string_writer);
		writer.setIndent("\t");
		Gson gsonBuilder = new GsonBuilder().setPrettyPrinting().create();
		gsonBuilder.toJson(element, writer);
		return string_writer.toString();
	}
	
	private String get_rarity(JsonObject item) {
		if (item.has("rarity"))
			return item.get("rarity").getAsString();
		return "";
	}
	
	private Attribute rarity_color(JsonObject item) {
		return rarity_color(get_rarity(item));
	}
	private Attribute rarity_color(String rarity) {
		return switch (rarity) {
			case "Normal" -> Attribute.TEXT_COLOR(200, 200, 200);
			case "Magic" -> Attribute.TEXT_COLOR(136, 136, 255);
			case "Rare" -> Attribute.TEXT_COLOR(255, 255, 119);
			case "Unique" -> Attribute.TEXT_COLOR(175, 96, 37);
//			case "Normal" -> Attribute.TEXT_COLOR(0xff, 0xff, 0xff); // rarity.dat colors
//			case "Magic" -> Attribute.TEXT_COLOR(0x40, 0x40, 0xff);
//			case "Rare" -> Attribute.TEXT_COLOR(0xff, 0xff, 0x30);
//			case "Unique" -> Attribute.TEXT_COLOR(0xd0, 0x90, 0x40);
			case null, default -> Attribute.CLEAR();
		};
	}
	
	private void addAttributes(JsonObject c) {
		stats.put("strength", c.get("base_str").getAsInt());
		stats.put("dexterity", c.get("base_dex").getAsInt());
		stats.put("intelligence", c.get("base_int").getAsInt());
	}
	private void base_attributes(String character_class) {
		JsonArray classes = skill_tree.get("classes").getAsJsonArray();
		boolean good = false;
		outer:
		for (var element : classes) {
			var c = element.getAsJsonObject();
			if (character_class.equals(c.get("name").getAsString())) {
				addAttributes(c);
				good = true;
				break;
			}
			var ascendancies = c.get("ascendancies").getAsJsonArray();
			for (var ascendancy_element : ascendancies) {
				var ascendancy = ascendancy_element.getAsJsonObject();
				if (character_class.equals(ascendancy.get("name").getAsString())) {
					addAttributes(c);
					good = true;
					break outer;
				}
			}
		}
		assert good : "character.class was not found in skill_tree.classes";
	}
	
	enum StatType {
		additive, additive_multiplier, multiplier
	}
	
	static class Stat {
		int value;
		String source;
		StatType type;
	}
	
	Map<String, List<Stat>[]> stats1 = new LinkedHashMap<>();
	void stat(String name, int value) {
		stat(name, value, "base", StatType.additive);
	}
	void stat(String name, int value, String source, StatType type) {
		if (!stats1.containsKey(name)) {
			//noinspection unchecked
			ArrayList<Stat>[] arr = new ArrayList[StatType.values().length];
			stats1.put(name, arr);
			for (int i = 0; i < StatType.values().length; i++)
				arr[i] = new ArrayList<>();
			
		}
		List<Stat> arr = stats1.get(name)[type.ordinal()];
		var s = new Stat();
		s.value = value;
		s.source = source;
		s.type = type;
		arr.add(s);
	}
	double calculate_stat(List<Stat>[] stat) {
		double out = stat[StatType.additive.ordinal()].stream().mapToInt(s -> s.value).sum();
//		out *= stat[StatType.additive_multiplier.ordinal()].stream().mapToInt(s -> 1 + s.value).sum();
//		for (Stat s : stat[StatType.multiplier.ordinal()]) {
//			out *=
//		}
		return out;
	}
	
	// https://www.poewiki.net/wiki/Deal_with_the_Bandits
	// TODO Data/QuestStaticRewards.dat
	enum BanditChoice {
		Kraityn, Alira, Oak, Eramir;
		static BanditChoice of(JsonElement choice) {
			return choice.isJsonNull() ? of((String) null) : of(choice.getAsString());
		}
		static BanditChoice of(String choice) {
			for (var option : BanditChoice.values())
				if (option.name().toLowerCase().equals(choice))
					return option;
			return null;
		}
	}
	
	Map<String, Integer> stats = new TreeMap<>();
	
	// options
	int level;
	String character_class;
	// TODO resistance penalty
	BanditChoice bandit_choice = null;
	// TODO pantheon
	
	void calculate_stats(JsonObject character) {
//		stat("level", character.get("level").getAsInt());
		level = character.get("level").getAsInt();
		
		character_class = character.get("class").getAsString();
		// TODO resistance penalty
		bandit_choice = BanditChoice.of(character.get("passives").getAsJsonObject().get("bandit_choice"));
		// TODO pantheon
		
		base_attributes(character_class);
		
		for (var element : character.get("equipment").getAsJsonArray()) {
//		for (var element : character.get("inventory").getAsJsonArray()) {
			var item = element.getAsJsonObject();
			if ("Flask".equals(item.get("inventoryId").getAsString())) continue;
			System.out.println(colorize(
				item.get("baseType").getAsString(),
				rarity_color(item)
			));
			
			if (item.has("properties")) {
				for (var property_element : item.get("properties").getAsJsonArray()) {
					var property = property_element.getAsJsonObject();
					System.out.print("- " + property.get("name").getAsString());
					boolean first = true;
					for (var value_e : property.get("values").getAsJsonArray()) {
						var value = value_e.getAsJsonArray();
						if (first) {
							first = false;
							System.out.print(":");
						} else
							System.out.print(",");
						System.out.print(" " + value.asList().getFirst().getAsString());
					}
					System.out.println();
				}
			}
			List.of(
				"implicitMods",
				"explicitMods",
				"craftedMods",
				"enchantMods",
				"scourgeMods",
				"ultimatumMods",
				"crucibleMods"
			).forEach(mod_type ->
//				for_each_modifier(item, mod_type, this::apply_modifier)
				{
					if (!item.has(mod_type)) return;
					for (var modifier : item.get(mod_type).getAsJsonArray()) {
						System.out.print(modifier.getAsString());
						if (!"explicitMods".equals(mod_type))
							System.out.print(" (" + mod_type.substring(0, mod_type.length() - 4) + ")");
						System.out.println();
						
						var mod_stats = stat_descriptions.match_modifier(modifier.getAsString());
						for (var mod_stat : mod_stats) {
							for (var e : mod_stat.entrySet())
								System.out.println("\t" + e.getKey() + ": " + e.getValue());
							System.out.println();
						}
					}
				}
			);

//			if ("Iron Ring".equals(item.get("baseType").getAsString()))
//			if ("Chainmail Vest".equals(item.get("baseType").getAsString()))
//				System.out.println(prettyJson(item));
			System.out.println();
			break;
		}
		
		// print stats
		for (var e : stats.entrySet())
			System.out.println(e.getKey() + ": " + e.getValue());

//		for (var e : stats1.entrySet())
//			System.out.println(e.getKey() + ": " + calculate_stat(e.getValue()));
		
		nop();
	}
	
	static PoeAPI api = new PoeAPI();
	static JsonObject get_current_character() throws InterruptedException {
		var json = api.request("/character").getAsJsonObject();
		
		JsonArray characters = json.getAsJsonArray("characters");
		for (var element : characters) {
			var character = element.getAsJsonObject();
			if (character.has("current"))
				return character;
		}
		return null;
	}
	public static void main(String[] args) throws InterruptedException, FileNotFoundException {
		Duration auth_expiry = api.auth_expiry();
		if (!auth_expiry.isPositive())
			api.authorize();
		
		JsonObject json = null;
		String jsonFilePath = "character.json";
		if (Files.exists(Path.of(jsonFilePath))) {
			json = Utilities.loadJSON(jsonFilePath).getAsJsonObject();
		} else {
			var c = get_current_character();
			assert c != null;
			System.out.println(c);
			json = api.request("/character/" + c.get("name").getAsString()).getAsJsonObject();
			Utilities.saveJSON(jsonFilePath, json);
		}
		
		if (json == null) {
			System.err.println("json is null");
			return;
		}
		
		Build b = new Build();
		b.calculate_stats(json.get("character").getAsJsonObject());
	}
}
