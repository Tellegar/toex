package com.tellegar.toex_old;

import java.io.*;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Path;
import java.util.*;

import static java.lang.Math.max;

public class Extract {
	public static void nop() {}
	
	static void run(String... command) throws IOException, InterruptedException {
		run(true, command);
	}
	static void run(boolean redirect, String... command) throws IOException, InterruptedException {
		ProcessBuilder pb = new ProcessBuilder(command);
		if (redirect) pb
			.redirectOutput(ProcessBuilder.Redirect.INHERIT)
			.redirectError(ProcessBuilder.Redirect.INHERIT);
		
		Process proc = pb.start();
		Thread hook = new Thread(proc::destroy);
		Runtime.getRuntime().addShutdownHook(hook);
		proc.waitFor();
		Runtime.getRuntime().removeShutdownHook(hook);
	}
	static void run_bun_extract() throws IOException, InterruptedException {
		long start = System.nanoTime();
		String export_path = "export\\ggpk";
		run( // 184s
			"cmd.exe", "/c",
			"rmdir", "/s/q", export_path
		);
		run( // 120s
			"export/bin/bun_extract_file.exe", "extract-files", "--regex",
			"C:/Program Files (x86)/Steam/steamapps/common/Path of Exile",
			export_path, "^metadata.+|^data.+"
		);
		long end = System.nanoTime();
		System.out.println((end - start) / 1_000_000_000.);
	}
	
	// TODO run bun.bat before Description.extract
	public static void main(String[] args) throws IOException, InterruptedException {
//		run_bun_extract();
		
		List<Description> descriptions = Description.extract(Path.of("export/ggpk/metadata/statdescriptions/stat_descriptions.txt"));
//		Utilities.save_to_file(descriptions, "src/main/resources/ggpk-export/descriptions.ser");
		System.out.println("descriptions.size(): " + descriptions.size());
		
		StatDescriptions stat_descriptions = new StatDescriptions(descriptions);
		Utilities.save_to_file(stat_descriptions, "src/main/resources/ggpk-export/stat_descriptions.ser");
		
		
		System.out.println("downloading schema.min.json"); // for DatTable
		try (HttpClient client = HttpClient.newBuilder()
			.followRedirects(HttpClient.Redirect.NORMAL)
			.build()) {
			HttpRequest request = HttpRequest.newBuilder()
				.GET()
				.uri(URI.create("https://github.com/poe-tool-dev/dat-schema/releases/download/latest/schema.min.json"))
				.build();
			
			HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
			
			try (FileWriter file = new FileWriter("src/main/resources/schema.min.json")) {
				file.write(response.body());
			}
		} catch (IOException | InterruptedException e) {
			throw new RuntimeException(e);
		}
		
		nop();
	}
}
