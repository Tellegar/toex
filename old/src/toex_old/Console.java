package com.tellegar.toex_old;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;

import static com.sun.jna.platform.win32.WinNT.*;
import static com.sun.jna.platform.win32.Wincon.*;
import static com.tellegar.toex_old.Kernel32.kernel32;
import static com.tellegar.toex_old.Kernel32.getLastErrorMessage;

import java.io.*;
import java.nio.charset.StandardCharsets;


interface Kernel32 extends com.sun.jna.platform.win32.Kernel32 {
	Kernel32 kernel32 = Native.load("kernel32", Kernel32.class);
	
	static String getLastErrorMessage() {
		int bufferSize = 160;
		byte[] data = new byte[bufferSize];
		kernel32.FormatMessageW(
			FORMAT_MESSAGE_FROM_SYSTEM,
			null,
			kernel32.GetLastError(),
			0,
			data,
			bufferSize,
			null
		);
		return (new String(data, StandardCharsets.UTF_16LE)).trim();
	}
	
	HANDLE CreateConsoleScreenBuffer(
		int dwDesiredAccess,
		int dwShareMode,
		SECURITY_ATTRIBUTES lpSecurityAttributes,
		int dwFlags,
		Pointer lpScreenBufferData
	);
	boolean SetConsoleActiveScreenBuffer(
		HANDLE hConsoleOutput
	);
	boolean WriteConsoleW(
		HANDLE hConsoleOutput,
		char[] lpBuffer,
		int nNumberOfCharsToWrite,
		IntByReference lpNumberOfCharsWritten,
		LPVOID lpReserved
	);
	boolean FormatMessageW(
		int dwFlags,
		Pointer lpSource,
		int dwMessageId,
		int dwLanguageId,
		byte[] lpBuffer,
		int nSize,
		Pointer Arguments
	);
}


/**
 * Implements similar functionality to ncurses.
 * <br>
 */
public class Console {
	private class ConsoleWriter extends Writer {
		IntByReference writtenChars = new IntByReference();
		@Override
		public void write(char[] cbuf, int off, int len) throws IOException {
			char[] text = cbuf;
			if (off != 0) {
				text = new char[len];
				System.arraycopy(cbuf, off, text, 0, len);
			}
			
			synchronized (this.lock) {
				if (kernel32.WriteConsoleW(new_console, text, len, writtenChars, null))
					throw new IOException("Failed to write to console: " + getLastErrorMessage());
			}
		}
		@Override
		public void flush() throws IOException {}
		@Override
		public void close() throws IOException {}
	}
	
	final HANDLE original_console, new_console;
	PrintWriter out;
	
	/**
	 * Creates new console screen buffer and switches to it
	 *
	 * @throws IOException when current console is not Windows
	 */
	Console() throws IOException {
		original_console = kernel32.GetStdHandle(STD_OUTPUT_HANDLE);
		IntByReference lpMode = new IntByReference();
		if (!kernel32.GetConsoleMode(original_console, lpMode)) {
			System.err.println("GetConsoleMode: " + getLastErrorMessage());
			throw new IOException("not a Windows terminal");
		}
		
		// ESC[?1049h    Use Alternate Screen Buffer    Switches to a new alternate screen buffer.
		// ESC[?1049l    Use Main Screen Buffer         Switches to the main buffer.
		// seems to work better than ANSI solution
		new_console = kernel32.CreateConsoleScreenBuffer(
			GENERIC_READ | GENERIC_WRITE,
			FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
			null,
			1, // CONSOLE_TEXTMODE_BUFFER
			null
		);
		kernel32.SetConsoleActiveScreenBuffer(new_console);
		
		out = new PrintWriter(new ConsoleWriter());
	}
	void close() {
		kernel32.SetConsoleActiveScreenBuffer(original_console);
		out.close();
		out = null;
	}
	
	void cursor_show(boolean show) {
		if (show)
			out.write("\033[?25h");
		else
			out.write("\033[?25l");
	}
	
	void color_text(int r, int g, int b) {
		out.print("\033[38;2;%d;%d;%dm".formatted(r, g, b));
	}
	void color_back(int r, int g, int b) {
		out.print("\033[48;2;%d;%d;%dm".formatted(r, g, b));
	}
	void color_reset() {
		out.print("\033[0m");
	}
	
	void clear() {
		out.print("\033[2J\033[3J\033[H");
	}
	void move_to(int x, int y) {
		out.print("\033[%d;%dH".formatted(y + 1, x + 1));
	}
	
	public record Size(int width, int height) {}
	Size get_size() {
		CONSOLE_SCREEN_BUFFER_INFO info = new CONSOLE_SCREEN_BUFFER_INFO();
		kernel32.GetConsoleScreenBufferInfo(new_console, info);
		return new Size(
			info.srWindow.Right - info.srWindow.Left + 1,
			info.srWindow.Bottom - info.srWindow.Top + 1
		);
	}
}
