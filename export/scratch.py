import os, shlex

# magic = magic.Magic(mime_encoding=True)

# print(*dirs)
# art minimap shaders audio data metadata cachedhlslshaders

def parse_list_files():
	files = []
	with open("list_files.log", encoding="utf-16") as f:
		while True:
			line = f.readline()[:-1]
			if not line: break
			if any(line.startswith(root_dir) for root_dir in "data metadata".split()):
				line = os.path.join("ggpk", line)
				files.append(line)
	# print(*files, sep="\n")
	return files

# def dump_hex(file, out, width=4):
# 	with open(file, "rb") as fin, open(out, "w") as fout:
# 		data = fin.read()
# 		h = data.hex(' ').split(' ')
# 		for i in range(0, len(h), width):
# 			fout.write(" ".join(h[i:i + width]))
# 			fout.write("  ")
# 			fout.write("".join([chr(c) if c < 128 and chr(c).isprintable() else "." for c in data[i:i + width]]))
# 			fout.write("\n")
# 			pass
#
# def find(string):
# 	for file in files:
# 		is_text = file.endswith(".txt")
# 		encoding = magic.from_buffer(open(file, "rb").read(2048)) if is_text else None
# 		with open(
# 				file,
# 				"r" if is_text else "rb",
# 				encoding=encoding
# 		) as f:
# 			data = f.read()
# 			if is_text:
# 				x = data.find(string)
# 			else:
# 				x = data.find(string.encode("utf-16le"))
# 			if x != -1:
# 				print(file, x)

# find("to Strength")
# find("additional_strength")

# dump_hex("data/characterpaneldescriptionmodes.dat64", "characterpaneldescriptionmodes.txt")
# dump_hex("data/characterpaneldescriptionmodes.datl64", "characterpaneldescriptionmodes_l.txt")
# dump_hex("data/mods.dat64", "mods.txt")

# with open("ggpk/data/characterpaneldescriptionmodes.dat64", "rb") as f:
# 	data = f.read()
#
# 	def find(string):
# 		return data.find(string.encode("utf-16le"))
#
# 	# chars = {}
# 	# for i, c in enumerate(data):
# 	# 	c = chr(c)
# 	# 	if c.isprintable():
# 	# 		d = chars.get(c, dict(count=0, positions=[]))
# 	# 		d["count"] += 1
# 	# 		d["positions"].append(i)
# 	# 		chars[c] = d
# 	# print(chars)
# 	strs = [
# 		"FlatValue",
# 		"PercentageValue",
# 		"Range",
# 		"CurrentOfMax",
# 		"Yes",
# 		"Resistance",
# 		"RangeOrMin",
# 		"PercentModifier",
# 		"PercentModifierFinal",
# 		"Seconds",
# 		"FlatValueFromPercentValue",
# 		"Metres"
# 	]
# 	for s in strs:
# 		print(s, find(s))
# 	pass

def parse_descriptions(file, encoding="UTF-16LE"):
	import re
	out = []
	gen_match_set = set()
	with open(file, "r", encoding=encoding) as f:
		state = 0
		description_or_language, variables, count, generators = range(4)
		if f.read(1) != '\ufeff': f.seek(0)
		lang = ""
		while True:
			line = f.readline()
			if not line: break
			line = line.rstrip()
			
			if state == description_or_language:
				if line == "": continue
				if line == "description":
					next_state = variables
					out.append({})
					lang = ""
				elif line.strip().startswith("lang "):
					lang = line.strip().replace("lang ", "")[1:-1]
					next_state = count
				else:
					print(line)
			if state == variables:
				if line == "": continue
				c, *v = line.split()
				assert int(c) == len(v)
				out[-1]["variables"] = v
				next_state = count
			if state == count:
				if line == "": continue
				c = int(line)
				next_state = generators
				out[-1]["generators"] = out[-1].get("generators", [])
			if state == generators:
				if line == "": continue
				if c:
					c -= 1
					gen = [lang, *line.lstrip().split(None, len(out[-1]["variables"]))]
					
					assert gen[-1][0] == '"'
					assert gen[-1].find('"', gen[-1].find('"', 1) + 1) == -1  # only two "
					quotes = [i for i, c in enumerate(gen[-1]) if c == '"']
					extra = gen[-1][quotes[-1]:].split()[1:]
					gen[-1] = gen[-1][1:quotes[-1]]
					gen += extra
					
					gen_match_set.update(re.findall(r"\{.+?[\}\]]", gen[-1]))
					out[-1]["generators"].append(gen)
				if c == 0:
					next_state = description_or_language
			state = next_state
	return out, gen_match_set

if __name__ == "__main__":
	descriptions, a = parse_descriptions("ggpk/metadata/statdescriptions/stat_descriptions.txt")
	# import json
	# print(json.dumps(descriptions, indent=2))
	# def f(description):
	# 	print(description["variables"])
	# 	for g in description["generators"]:
	# 		lang, g = g
	# 		if lang:
	# 			print(f"{lang}: {g}")
	# 		else:
	# 			print(g)
	# f(descriptions[4])
	s = [d for d in descriptions if d["variables"] == ["additional_strength"]][0]
	d = [d for d in descriptions if d["variables"] == ["additional_dexterity"]][0]
	i = [d for d in descriptions if d["variables"] == ["additional_intelligence"]][0]
	chill_enemy_when_hit_duration_ms = [
		d for d in descriptions if d["variables"] == ["chill_enemy_when_hit_duration_ms"]
	][0]
	
	i = 0
	# my_map = dict()
	# for d in descriptions:
	# 	n = len(d["variables"])
	# 	for gen in d["generators"]:
	# 		if gen[0] == "":
	# 			print(gen[1 + n])
	# 			i += 1
	# 	if i > 100: break;
	
	# {0]から{1} {6} {1} {5} {5:+d} {:+d} {0} {4} {:d} {0:d} {0:+d} {1:+d} {2} {3} {2:+d} {8}
	print(*a)
	# for gen in s["generators"]:
	# 	print(gen[-1])
	pass
