https://github.com/zao/ooz/releases

using `.\bun_extract_file.exe [--help]` you can extract data files from **Path of Exile** `Content.ggpk`

`GGPK_OR_STEAM_DIR` can be either:
- `"C:\Program Files (x86)\Grinding Gear Games\Path of Exile\Content.ggpk"` standalone
- `"C:\Program Files (x86)\Steam\steamapps\common\Path of Exile"` steam\
  (note that the path cannot end with `\`)

other tools: https://www.poewiki.net/wiki/List_of_Path_of_Exile_community_applications#Development_resources