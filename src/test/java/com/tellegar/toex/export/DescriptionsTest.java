package com.tellegar.toex.export;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.tellegar.toex.export.Descriptions.*;
import static org.junit.jupiter.api.Assertions.*;

class DescriptionsTest {
	private static Descriptions desc;
	private static Random rand = new Random();
	private static int seed;
	
	@BeforeAll
	static void set_up() throws IOException {
		Stats.init();
		Mods.init();
		english_only = false;
		desc = parse_file(Path.of(Export.resources + "stat_descriptions.txt"));
		rand.setSeed(47);
	}
	
	void set_up_rand() {
		seed = new Random().nextInt();
		seed = 47;
		rand.setSeed(seed);
		System.out.println("seed: " + seed);
	}
	
	@Test
	void scope_of() {
		class Asserter {
			void assertion(String scope_str, Scope scope) {
				assertEquals(scope, Scope.of(scope_str), "scopes do not match");
			}
			
			void f(String scope, Integer low, Integer high) {
				assertion(scope, new Scope.Range(low, high));
			}
			
			void f(String scope, int not_value) {
				assertion(scope, new Scope.NotValue(not_value));
			}
		}
		Asserter asserter = new Asserter();
		
		
		asserter.f("21|40", 21, 40);
		asserter.f("21|40", 21, 40);
		asserter.f("#|99", null, 99);
		asserter.f("#|-1", null, -1);
		asserter.f("-10", -10, -10);
		asserter.f("#|-2", null, -2);
		asserter.f("100|#", 100, null);
		asserter.f("10|#", 10, null);
		asserter.f("#|-11", null, -11);
		asserter.f("81|100", 81, 100);
		asserter.f("#|-10", null, -10);
		asserter.f("1|#", 1, null);
		asserter.f("#|1", null, 1);
		asserter.f("5|#", 5, null);
		asserter.f("10", 10, 10);
		asserter.f("11", 11, 11);
		asserter.f("12", 12, 12);
		asserter.f("#|60", null, 60);
		asserter.f("13", 13, 13);
		asserter.f("#", null, null);
		asserter.f("14", 14, 14);
		asserter.f("-1", -1, -1);
		asserter.f("15", 15, 15);
		asserter.f("16", 16, 16);
		asserter.f("1|1|#", 1, null); // probably an error in stat_descriptions.txt
		asserter.f("17", 17, 17);
		asserter.f("19", 19, 19);
		asserter.f("!0", 0);
		asserter.f("0", 0, 0);
		asserter.f("1", 1, 1);
		asserter.f("2", 2, 2);
		asserter.f("200", 200, 200);
		asserter.f("2|99", 2, 99);
		asserter.f("3", 3, 3);
		asserter.f("4", 4, 4);
		asserter.f("5", 5, 5);
		asserter.f("99|100", 99, 100);
		asserter.f("6", 6, 6);
		asserter.f("7", 7, 7);
		asserter.f("8", 8, 8);
		asserter.f("20|35", 20, 35);
		asserter.f("9", 9, 9);
		asserter.f("61|#", 61, null);
		asserter.f("25|#", 25, null);
		asserter.f("21", 21, 21);
		asserter.f("22", 22, 22);
		asserter.f("23", 23, 23);
		asserter.f("24", 24, 24);
		asserter.f("25", 25, 25);
		asserter.f("-9|#", -9, null);
		asserter.f("1000", 1000, 1000);
		asserter.f("61|80", 61, 80);
		asserter.f("-1|1", -1, 1);
		asserter.f("1|100", 1, 100);
		asserter.f("101|#", 101, null);
		asserter.f("1|99", 1, 99);
		asserter.f("15|24", 15, 24);
		asserter.f("0|#", 0, null);
		asserter.f("2|#", 2, null);
		asserter.f("99|#", 99, null);
		asserter.f("50|75", 50, 75);
		asserter.f("30", 30, 30);
		asserter.f("1001|#", 1001, null);
		asserter.f("4000", 4000, 4000);
		asserter.f("41|60", 41, 60);
		asserter.f("10|19", 10, 19);
		asserter.f("100", 100, 100);
		asserter.f("1|20", 1, 20);
		asserter.f("20|#", 20, null);
		asserter.f("-99|-1", -99, -1);
		asserter.f("0|99", 0, 99);
		asserter.f("#|-100", null, -100);
	}
	
	// format patterns
	
	@Test
	void match_modifier_fixed() {
		class Asserter {
			final void f(String modifier, List<Map<String, Object>> expected) {
				List<Map<Integer, Scope>> expected_interpretations = expected
					.stream()
					.map(mod_stat -> mod_stat
						.entrySet()
						.stream()
						.collect(Collectors.toMap(
							e -> Stats.id_to_row.get(e.getKey()),
							e -> {
								if (e.getValue() instanceof String scope_str)
									return Scope.of(scope_str);
								else if (e.getValue() instanceof Integer value)
									return Value.of(value);
								else
									throw new RuntimeException("unhandled value `" + e.getValue() + "`");
							}
						))
					).collect(Collectors.toList());
				
				var interpretations = desc.match_modifier(modifier);
				
				System.out.println("* " + modifier);
				for (var mod_stat : interpretations) {
					for (var entry : mod_stat.entrySet())
						System.out.println(entry.getKey() + ": " + entry.getValue());
					System.out.println();
				}
				System.out.println();
				
				assertEquals(
					expected_interpretations,
					interpretations,
					"failed for `" + modifier + "`"
				);
			}
			
			final Map<Integer, Scope> mod_stat(Object... stats) {
				assert stats.length % 2 == 0 : "mod_stats needs to be pairs";
				Map<Integer, Scope> mod_stat = new HashMap<>();
				for (int i = 0; i < stats.length; i += 2) {
					Integer stat = Stats.id_to_row.get((String) stats[i]);
					Scope scope;
					if (stats[i + 1] instanceof String scope_str)
						scope = Scope.of(scope_str);
					else if (stats[i + 1] instanceof Integer value)
						scope = Value.of(value);
					else
						throw new RuntimeException("unhandled value `" + stats[i + 1] + "`");
					mod_stat.put(
						stat,
						scope
					);
				}
				return mod_stat;
			}
		}
		Asserter asserter = new Asserter();
		
		
		asserter.f("+9 to Strength", List.of(Map.of(
			"additional_strength", 9
		)));
		asserter.f("Rare and Unique Monsters in Area are Possessed by 2 to 3 Tormented Spirits and their Minions are Touched", List.of(Map.of(
			"memory_line_minimum_possessions_of_rare_unique_monsters", 2,
			"memory_line_maximum_possessions_of_rare_unique_monsters", 3
		)));
		asserter.f("Bathed in the blood of 529 sacrificed in the name of Xibaqua\nPassives in radius are Conquered by the Vaal", List.of(Map.of(
			"local_unique_jewel_alternate_tree_version", "1|1",
			"local_unique_jewel_alternate_tree_seed", 529,
			"local_unique_jewel_alternate_tree_keystone", "1|1",
			"local_unique_jewel_alternate_tree_internal_revision", "#"
		)));
		asserter.f("Historic", List.of(Map.of(
			"local_is_alternate_tree_jewel", "#"
		)));
		asserter.f("Split Arrow fires an additional arrow", List.of(Map.of(
			"split_arrow_number_of_additional_arrows", "#|1"
		)));
		asserter.f("Split Arrow fires 2 additional arrows", List.of(Map.of(
			"split_arrow_number_of_additional_arrows", 2
		)));
		asserter.f("47% increased Explicit Ailment Modifier magnitudes", List.of(Map.of(
			"heist_enchantment_ailment_mod_effect_+%", 47
		)));
		asserter.f("47% reduced Explicit Ailment Modifier magnitudes", List.of(Map.of(
			"heist_enchantment_ailment_mod_effect_+%", -47
		)));
		asserter.f("-27 to maximum Sockets", List.of(Map.of(
			"local_maximum_sockets_+", -27
		)));
		asserter.f("Attacks cause Bleeding", List.of(Map.of(
			"cannot_cause_bleeding", "0|0",
			"bleed_on_hit_with_attacks_%", "#",
			"global_bleed_on_hit", "!0"
		), Map.of(
			"cannot_cause_bleeding", "0|0",
			"bleed_on_hit_with_attacks_%", "100|#",
			"global_bleed_on_hit", "0|0"
		)));
		asserter.f("+10 to maximum Sockets", List.of(Map.of(
			"local_maximum_sockets_+", 10
		)));
		asserter.f("-5 to maximum Sockets", List.of(Map.of(
			"local_maximum_sockets_+", -5
		)));
		asserter.f("Breach Splinters have 0.24% chance to drop as Breachstones instead", List.of(Map.of(
			"map_breach_splinters_drop_as_stones_permyriad", 24
		)));
	}
	
	@Test
	void match_modifier_random_via_mods() {
		set_up_rand();
		
		Map<Integer, List<Descriptions.Description>> descriptions_by_stat = new HashMap<>();
		for (var description : desc.descriptions) {
			for (int stat : description.variables) {
				if (!descriptions_by_stat.containsKey(stat))
					descriptions_by_stat.put(stat, new ArrayList<>());
//				else
//					System.out.println(stat);
				descriptions_by_stat.get(stat).add(description);
			}
		}
		
		for (int row = 0; row < Mods.mods.row_count; row++) {
			Mods.Stats mod_row = Mods.get_stats(row);
			
			List<Descriptions.Description> list = new ArrayList<>();
			for (int i = 0; i < mod_row.stats().length; i++) {
				int stat = mod_row.stats()[i];
				if (mod_row.ranges()[i].is_zero())
					continue;
				if (!descriptions_by_stat.containsKey(stat))
					continue;
				for (Descriptions.Description description : descriptions_by_stat.get(stat)) {
					if (Arrays.stream(description.variables)
						.allMatch(var -> Arrays.stream(mod_row.stats())
							.anyMatch(s -> var == s))
					)
						list.add(description);
					else {
						System.out.println(
							Arrays.toString(description.variables) +
								" is not subset of " +
								Arrays.toString(mod_row.stats())
						);
					}
				}
			}
			
			Map<Integer, Value> values = IntStream
				.range(0, mod_row.ranges().length)
				.boxed()
//				.filter(i -> !mod_row.ranges()[i].is_zero())
				.collect(Collectors.toMap(
					i -> mod_row.stats()[i],
					i -> Value.of(rand.nextInt(
						mod_row.ranges()[i].low(),
						mod_row.ranges()[i].high() + 1
					))
				));
			
			System.out.println();
			System.out.println(Mods.mods.get("Id", row));
			System.out.println(values);
			if (list.isEmpty()) {
				System.out.print("\033[38;2;255;255;0m");
				System.out.println("list is empty");
				System.out.print("\033[0m");
			}
			
			for (var description : list) {
				for (var generator : description.generators) {
					if (!"English".equals(generator.language)) continue;
					
					if (!IntStream.range(0, description.variables.length)
						.allMatch(i -> {
							Scope scope = generator.variable_scopes[i];
							Value value = values.get(description.variables[i]);
							return scope.is_satisfied(value);
						})) continue;
					
					Scope[] mod_values = Arrays.stream(description.variables)
						.mapToObj(values::get)
						.toArray(Scope[]::new);
					
					Descriptions.apply_arguments(
						mod_values,
						generator.arguments,
						false
					);
					
					String modifier = format_p.matcher(generator.format_string).replaceAll(
						match_result -> {
							FormatPattern fp = FormatPattern.of(match_result.group());
							return String.valueOf(mod_values[fp.index()]);
						}
					);
					
					System.out.println(modifier);
					var interpretations = desc.match_modifier(modifier);
					
					int number_of_good = 0;
					for (var interpretation : interpretations) {
						boolean good = true;
						for (int key : interpretation.keySet()) {
							if (!values.containsKey(key)) {
								good = false;
								break;
							}
							Value expected = values.get(key);
							Scope scope = interpretation.get(key);
							if (!scope.is_satisfied(expected)) {
								good = false;
								break;
							}
						}
						if (good)
							number_of_good++;
						System.out.print(good ? "\033[38;2;0;255;0m" : "\033[38;2;255;0;0m");
						System.out.println(interpretation);
						System.out.print("\033[0m");
					}
					if (modifier.equals("Cannot be Poisoned"))
						assertEquals(number_of_good, 2);
					else
						assertEquals(number_of_good, 1);
				}
			}
		}
	}
	
	@Test
	void match_modifier_random_via_descriptions() {
		set_up_rand();
		
		for (var description : desc.descriptions) {
			for (var generator : description.generators) {
				
				// values expected to be returned from match_modifier
				Scope[] expected_variables = Arrays.copyOf(generator.variable_scopes, generator.variable_scopes.length);
				String modifier = generator.format_string;
				// actual values in modifier
				Value[] used_variables = new Value[expected_variables.length];
				
				for (var fp : generator.format_patterns) {
					int value = 0;
					if (used_variables[fp.index()] != null) continue;
					if (expected_variables[fp.index()] instanceof Scope.Range range) {
						Integer low = range.low(), high = range.high();
						if (low == null && high == null) {
							low = "+d".equals(fp.argument()) ? 0 : -100;
							high = 100;
						} else if (low == null)
							low = high - 99;
						else if (high == null)
							high = low + 100;
						value = rand.nextInt(low, high + 1);
					} else if (expected_variables[fp.index()] instanceof Scope.NotValue not_value)
						value = rand.nextInt(not_value.value() + 1, not_value.value() + 100);
					else fail("unhandled type: " + expected_variables[fp.index()].getClass());
//					expected_variables[fp.index()] = new Scope.Range(value, value);
					expected_variables[fp.index()] = Value.of(value);
					used_variables[fp.index()] = Value.of(value);
				}
				
				apply_arguments(used_variables, generator.arguments, false);
				
				modifier = format_p.matcher(modifier).replaceAll(match_result -> {
					FormatPattern fp = FormatPattern.of(match_result.group());
					Value value = used_variables[fp.index()];
					if (fp.argument() != null && fp.argument().startsWith("+") && value.value.signum() != -1)
						return "+" + value;
					return value.toString();
				});
				
				Map<Integer, Scope> expected_mod_stat = new HashMap<>();
				for (int i = 0; i < expected_variables.length; i++)
					expected_mod_stat.put(description.variables[i], expected_variables[i]);
				
				if ("English".equals(generator.language)) {
					System.out.println(modifier);
					System.out.println(expected_mod_stat);
				}
				
				var list = desc.match_modifier(modifier);
				if (list != null) {
					int count = 0;
					for (var mod_stat : list) {
						boolean equals = expected_mod_stat.equals(mod_stat);
						if ("English".equals(generator.language)) {
							System.out.print(equals ? "\033[38;2;0;255;0m" : "\033[38;2;255;0;0m");
							System.out.println(mod_stat);
							System.out.print("\033[0m");
						}
						if (equals)
							count++;
					}
					assertEquals(1, count, "exactly one mod-stat should be equal to expected");
				} else
					fail("no mod_stat found");
				if ("English".equals(generator.language))
					System.out.println();
			}
		}
	}
}