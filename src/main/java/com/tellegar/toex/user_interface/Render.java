package com.tellegar.toex.user_interface;

public interface Render {
	record Color(int r, int g, int b) {
		public String color_text() {
			return EscapeSequences.color_text(r, g, b);
		}
		public String color_back() {
			return EscapeSequences.color_back(r, g, b);
		}
		
		public static final String text = EscapeSequences.color_text(188, 190, 196);
//		public static final String text = EscapeSequences.color_text(255, 255, 255);
		
		public static final String back = EscapeSequences.color_back(20, 21, 22);
	}
	
	/**
	 * @param width expected width of the output
	 * @return String, may contain multiple lines, each should be of {@code width} length
	 */
	default String render(int width) {
		return String.format("%1$-" + width + "s", this);
	}
}
