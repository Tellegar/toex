package com.tellegar.toex.user_interface;

import lc.kra.system.keyboard.GlobalKeyboardHook;
import lc.kra.system.keyboard.event.GlobalKeyEvent;
import lc.kra.system.keyboard.event.GlobalKeyListener;
import lc.kra.system.mouse.GlobalMouseHook;
import lc.kra.system.mouse.event.GlobalMouseEvent;
import lc.kra.system.mouse.event.GlobalMouseListener;

import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

public class KeyListener {
	GlobalKeyboardHook keyboard_hook;
	GlobalMouseHook mouse_hook;
	
	private static final Map<Integer, String> vk_name;
	
	static {
		vk_name = new HashMap<>();
		for (var f : GlobalKeyEvent.class.getDeclaredFields()) {
			int modifiers = f.getModifiers();
			if (Modifier.isPublic(modifiers) &&
				Modifier.isStatic(modifiers) &&
				Modifier.isFinal(modifiers) &&
				f.getType() == int.class) {
				try {
					vk_name.put(f.getInt(null), f.getName());
				} catch (IllegalAccessException e) {
					throw new RuntimeException(e);
				}
			}
		}
	}
	public static String keyevent_to_string(GlobalKeyEvent event) {
		if (event == null)
			return "";
		if (vk_name.containsKey(event.getVirtualKeyCode())) {
			String name = vk_name.get(event.getVirtualKeyCode());
			String[] arr = event.toString().split("\\s", 2);
			return name + " " + arr[1];
		}
		return event.toString();
	}
	
	public interface Hook extends GlobalKeyListener, GlobalMouseListener {
		@Override
		default void keyPressed(GlobalKeyEvent event) {}
		@Override
		default void keyReleased(GlobalKeyEvent event) {}
		
		@Override
		default void mousePressed(GlobalMouseEvent event) {}
		@Override
		default void mouseReleased(GlobalMouseEvent event) {}
		@Override
		default void mouseMoved(GlobalMouseEvent event) {}
		@Override
		default void mouseWheel(GlobalMouseEvent event) {}
	}
	public KeyListener() {
		boolean raw = true;
		keyboard_hook = new GlobalKeyboardHook(raw);
		mouse_hook = new GlobalMouseHook(raw);
	}
	
	void stop() {
		synchronized (lock) {
			lock.notifyAll();
		}
	}
	
	public void run(Hook hook) throws InterruptedException {
		keyboard_hook.addKeyListener(hook);
		mouse_hook.addMouseListener(hook);
		try {
			synchronized (lock) {
				lock.wait();
			}
		} finally {
			keyboard_hook.shutdownHook();
			mouse_hook.shutdownHook();
		}
	}
	
	private static final Object lock = new Object();
	
	public static void main(String[] args) throws InterruptedException {
		KeyListener kl = new KeyListener();
		kl.run(new Hook() {
			@Override
			public void keyPressed(GlobalKeyEvent event) {
				if (event.getVirtualKeyCode() == GlobalKeyEvent.VK_L)
					kl.stop();
			}
		});
	}
}