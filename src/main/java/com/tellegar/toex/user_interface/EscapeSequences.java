package com.tellegar.toex.user_interface;

/**
 * terminal escape sequences
 * <br>
 * link <a href=https://learn.microsoft.com/en-us/windows/console/console-virtual-terminal-sequences>
 * microsoft: console-virtual-terminal-sequences
 * </a>
 */
public interface EscapeSequences {
	// Cursor Positioning
	static String cursor_up(int n) {
		return n == 0 ? "" : "\033[%dA".formatted(n);
	}
	static String cursor_down(int n) {
		return n == 0 ? "" : "\033[%dB".formatted(n);
	}
	static String cursor_right(int n) {
		return n == 0 ? "" : "\033[%dC".formatted(n);
	}
	static String cursor_left(int n) {
		return n == 0 ? "" : "\033[%dD".formatted(n);
	}
	//static String cursor_next_line(int n) {
	//	return "\033[%dE".formatted(n);
	//}
	//static String cursor_previous_line(int n) {
	//	return "\033[%dF".formatted(n);
	//}
	String cursor_horizontal_absolute = "\033[G";
	static String cursor_horizontal_absolute(int n) {
		return "\033[%dG".formatted(n);
	}
	//static String cursor_vertical_absolute(int n) {
	//	return "\033[%dd".formatted(n);
	//}
	/**
	 * @param x zero indexed
	 * @param y zero indexed
	 */
	static String cursor_position(int x, int y) {
		return "\033[%d;%dH".formatted(y + 1, x + 1);
	}
	
	// Cursor Visibility TODO
	// Cursor Shape
	// Viewport Positioning
	
	// Text Modification
	String clear = "\033[2J\033[3J\033[H";
	
	// Text Formatting
	String format_reset = "\033[m";
	String format_negative = "\033[7m";
	String format_positive = "\033[27m";
//	String format_reset_text = "\033[39m"; // use Color.text instead
	String format_reset_back = "\033[49m";
	
	// Extended Colors
	static String color_text(int r, int g, int b) {
		return "\033[38;2;%d;%d;%dm".formatted(r, g, b);
	}
	static String color_back(int r, int g, int b) {
		return "\033[48;2;%d;%d;%dm".formatted(r, g, b);
	}
	
}
