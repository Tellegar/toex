package com.tellegar.toex.user_interface;

import java.util.Iterator;

/**
 * represents selection of items render-able to console
 *
 * @param <E> type of item
 */
public interface Selection<E extends Render> extends Iterable<E> {
	/**
	 * @return selected index
	 */
	int selected();
	/**
	 * @param selected index to select
	 */
	void select(int selected);
	/**
	 * @return selected item
	 */
	E select();
	
	default boolean is_selectable() {
		Iterator<E> it = iterator();
		if (it.hasNext()) it.next();
		return it.hasNext();
	}
}