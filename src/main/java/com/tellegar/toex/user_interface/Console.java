package com.tellegar.toex.user_interface;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;

import static com.sun.jna.platform.win32.WinNT.*;
import static com.sun.jna.platform.win32.Wincon.*;

import java.io.*;
import java.nio.charset.StandardCharsets;

import static com.tellegar.toex.user_interface.Kernel32.kernel32;
import static com.tellegar.toex.user_interface.Kernel32.getLastErrorMessage;

/**
 * Implements similar functionality to ncurses
 * <br>
 * Requires Windows terminal cmd/powershell with support for <a href="https://learn.microsoft.com/en-us/windows/console/console-reference">Windows API Console commands</a>
 */
public class Console {
	private final HANDLE original_console, new_console;
	public final PrintWriter out;
	
	/**
	 * Creates new console screen buffer and switches to it
	 * @throws IOException current terminal is not Windows terminal
	 */
	public Console() throws IOException {
		original_console = kernel32.GetStdHandle(STD_OUTPUT_HANDLE);
		IntByReference lpMode = new IntByReference();
		if (!kernel32.GetConsoleMode(original_console, lpMode)) {
			System.err.println("GetConsoleMode: " + getLastErrorMessage());
			throw new IOException("not a Windows terminal");
		}
		
		// TODO ENABLE_VIRTUAL_TERMINAL_PROCESSING SetConsoleMode
		//      terminal might support escape sequences but they might not be enabled
		
		// ESC[?1049h    Use Alternate Screen Buffer    Switches to a new alternate screen buffer.
		// ESC[?1049l    Use Main Screen Buffer         Switches to the main buffer.
		// seems to work better than escape sequence solution
		new_console = kernel32.CreateConsoleScreenBuffer(
			GENERIC_READ | GENERIC_WRITE,
			FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
			null,
			1, // CONSOLE_TEXTMODE_BUFFER
			null
		);
		kernel32.SetConsoleActiveScreenBuffer(new_console);
		
		out = new PrintWriter(new ConsoleWriter());
	}
	public void close() {
		kernel32.SetConsoleActiveScreenBuffer(original_console);
		kernel32.CloseHandle(new_console);
		out.close();
	}
	
	public record Size(int width, int height) {}
	public Size get_size() {
		CONSOLE_SCREEN_BUFFER_INFO info = new CONSOLE_SCREEN_BUFFER_INFO();
		kernel32.GetConsoleScreenBufferInfo(new_console, info);
		return new Size(
			info.srWindow.Right - info.srWindow.Left,
			info.srWindow.Bottom - info.srWindow.Top
		);
	}
	
	private class ConsoleWriter extends Writer {
		IntByReference writtenChars = new IntByReference();
		@Override
		public void write(char[] cbuf, int off, int len) throws IOException {
			char[] text = cbuf;
			if (off != 0) {
				text = new char[len];
				System.arraycopy(text, off, text, 0, len);
			}
			
			Object lock = this.lock;
			synchronized (lock) {
				if (kernel32.WriteConsoleW(new_console, text, len, writtenChars, null))
					throw new IOException("Failed to write to console: " + getLastErrorMessage());
			}
		}
		@Override
		public void flush() {}
		@Override
		public void close() {}
	}
}

interface Kernel32 extends com.sun.jna.platform.win32.Kernel32 {
	Kernel32 kernel32 = Native.load("kernel32", Kernel32.class);
	
	static String getLastErrorMessage() {
		int bufferSize = 160;
		byte[] data = new byte[bufferSize];
		kernel32.FormatMessageW(
			FORMAT_MESSAGE_FROM_SYSTEM,
			null,
			kernel32.GetLastError(),
			0,
			data,
			bufferSize,
			null
		);
		return (new String(data, StandardCharsets.UTF_16LE)).trim();
	}
	
	HANDLE CreateConsoleScreenBuffer(
		int dwDesiredAccess,
		int dwShareMode,
		SECURITY_ATTRIBUTES lpSecurityAttributes,
		int dwFlags,
		Pointer lpScreenBufferData
	);
	boolean SetConsoleActiveScreenBuffer(
		HANDLE hConsoleOutput
	);
	boolean WriteConsoleW(
		HANDLE hConsoleOutput,
		char[] lpBuffer,
		int nNumberOfCharsToWrite,
		IntByReference lpNumberOfCharsWritten,
		LPVOID lpReserved
	);
	boolean FormatMessageW(
		int dwFlags,
		Pointer lpSource,
		int dwMessageId,
		int dwLanguageId,
		byte[] lpBuffer,
		int nSize,
		Pointer Arguments
	);
}
