package com.tellegar.toex.user_interface;

import java.util.Iterator;
import java.util.List;

public class ArraySelection<E extends Render> implements Selection<E> {
	public List<E> list;
	public int selected = 0;
	
	@SafeVarargs
	public static <E extends Render> ArraySelection<E> of(E... elements) {
		ArraySelection<E> sel = new ArraySelection<>();
		sel.list = List.of(elements);
		return sel;
	}
	
	public static <E extends Render> ArraySelection<E> of(List<E> list) {
		ArraySelection<E> sel = new ArraySelection<>();
		sel.list = list;
		return sel;
	}
	
	@Override
	public int selected() {
		return selected;
	}
	@Override
	public void select(int selected) {
		if (list.isEmpty()) return;
		this.selected = Math.clamp(selected, 0, list.size() - 1);
	}
	@Override
	public E select() {
		if (list.isEmpty()) return null;
		return list.get(selected);
	}
	
	@Override
	public Iterator<E> iterator() {
		return list.iterator();
	}
}