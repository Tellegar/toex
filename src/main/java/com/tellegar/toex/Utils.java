package com.tellegar.toex;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.function.Function;
import java.util.stream.StreamSupport;

public class Utils {
	/**
	 * debugging function which does nothing
	 */
	public static void nop() {}
	
	public static JsonElement loadJSON(String path) throws FileNotFoundException {
		return JsonParser.parseReader(new FileReader(path));
	}
	public static void saveJSON(String path, JsonElement json) throws IOException {
		try (FileWriter file = new FileWriter(path)) {
			file.write(json.toString());
		}
	}
	
	public static double round(double value, int decimal_places) {
		double shift = Math.pow(10, decimal_places);
		return Math.round(value * shift) / shift;
	}
	
	public static <E> List<E> json_array_to_list(
		JsonArray array,
		Function<? super JsonElement, E> constructor) {
		return StreamSupport.stream(
				array.spliterator(), false
			).map(constructor)
			.toList();
	}
	public static List<JsonObject> json_array_to_list(JsonArray array) {
		return json_array_to_list(array, JsonElement::getAsJsonObject);
	}
}
