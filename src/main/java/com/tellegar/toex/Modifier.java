package com.tellegar.toex;

import com.tellegar.toex.export.Descriptions;
import com.tellegar.toex.export.Descriptions.Scope;
import com.tellegar.toex.export.Export;
import com.tellegar.toex.export.Stats;
import com.tellegar.toex.user_interface.EscapeSequences;
import com.tellegar.toex.user_interface.Render;
import com.tellegar.toex.user_interface.Selection;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Modifier implements Render, Selection<Modifier.Interpretation> {
	String modifier;
	String type;
	List<Map<Integer, Scope>> interpretations;
	int selected_interpretation = 0;
	
	static Descriptions descriptions = null;
	
	Modifier(String modifier, String type) {
		this.modifier = modifier;
		this.type = type;
		
		if (descriptions == null) {
			try {
				Stats.init();
//				Mods.init();
				descriptions = Descriptions.parse_file(Path.of(Export.resources + "stat_descriptions.txt"));
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		
		interpretations = descriptions.match_modifier(modifier);
	}
	
	// Render
	@Override
	public String toString() {
		if ("explicit".equals(type))
			return modifier;
		return modifier + " (" + type + ")";
	}
	
	@Override
	public String render(int width) {
		List<String> lines = Arrays.stream(toString().split("\n"))
			.map(line -> {
				assert !line.contains("\n") && !line.contains("\r");
				return String.format(" %1$-" + (width - 3) + "s ", line);
			})
			.toList();
		
		String out = IntStream.range(0, lines.size())
			.mapToObj(i -> (i == 0 ? '·' : ' ') + lines.get(i))
			.collect(Collectors.joining("\n"));
		
		if (interpretations == null || interpretations.isEmpty())
			out = EscapeSequences.color_text(255, 0, 0) + out;
		else if (interpretations.size() > 1)
			out = EscapeSequences.color_text(255, 128, 0) + out;
		else
			out = Color.text + out;
		
		String suffix_note = "[" + (interpretations == null ? 0 : interpretations.size()) + "] ";
		out = out.substring(0, out.length() - suffix_note.length()) + suffix_note;
		
		return out;
	}
	
	// Selection<Modifier.Interpretation>
	@Override
	public int selected() {
		return selected_interpretation;
	}
	@Override
	public void select(int selected) {
		selected_interpretation = Math.clamp(selected, 0, interpretations.size() - 1);
	}
	@Override
	public Interpretation select() {
		return null;
	}
	
	@Override
	public Iterator<Interpretation> iterator() {
//		if (interpretations.size() == 1) return Collections.emptyIterator();
		return IntStream
			.range(0, interpretations.size())
			.mapToObj(i -> new Interpretation(
				interpretations.get(i),
				selected_interpretation == i
			))
			.iterator();
	}
	
	public static class Interpretation implements Render {
		Map<Integer, Scope> interpretation;
		boolean is_selected;
		
		Interpretation(Map<Integer, Scope> interpretation, boolean is_selected) {
			this.interpretation = interpretation;
			this.is_selected = is_selected;
		}
		
		@Override
		public String render(int width) {
			return (is_selected? Color.text: EscapeSequences.color_text(100,100,100)) +
				String.format(" %1$-" + (width - 2) + "s ", interpretation);
		}
	}
}
