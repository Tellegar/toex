package com.tellegar.toex;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tellegar.toex.user_interface.*;
import lc.kra.system.keyboard.event.GlobalKeyEvent;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.tellegar.toex.Utils.loadJSON;
import static com.tellegar.toex.user_interface.EscapeSequences.*;

public class Main implements KeyListener.Hook {
	GlobalKeyEvent last_key_event;
	@Override
	public void keyPressed(GlobalKeyEvent event) {
//		if (last_key_event != null && last_key_event.toString().equals(event.toString())) {
//			last_key_event = event;
//			return;
//		}
		last_key_event = event;
		
		Selection<?> sel = selections.getLast();
		if (event.getVirtualKeyCode() == GlobalKeyEvent.VK_UP)
			sel.select(sel.selected() - 1);
		if (event.getVirtualKeyCode() == GlobalKeyEvent.VK_DOWN)
			sel.select(sel.selected() + 1);
		
		if (event.getVirtualKeyCode() == GlobalKeyEvent.VK_LEFT)
			if (selections.size() > 1)
				selections.removeLast();
		if (event.getVirtualKeyCode() == GlobalKeyEvent.VK_RIGHT)
			if (sel.select() instanceof Selection<?> next_sel && next_sel.is_selectable())
				selections.add(next_sel);
		
		if (List.of(
			GlobalKeyEvent.VK_UP,
			GlobalKeyEvent.VK_DOWN,
			GlobalKeyEvent.VK_LEFT,
			GlobalKeyEvent.VK_RIGHT
		).contains(event.getVirtualKeyCode()))
			render();
		
		if (event.getVirtualKeyCode() == GlobalKeyEvent.VK_L &&
			event.isControlPressed() &&
			event.isShiftPressed()
		) {
			console.out.print(EscapeSequences.cursor_position(0, console_size.height() - 2));
			console.out.print("loading character");
			try {
				load_character_equipment();
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
		}
		
		console.out.print(cursor_position(1, console_size.height() - 1));
		console.out.print(KeyListener.keyevent_to_string(last_key_event));
		console.out.print(cursor_horizontal_absolute);
	}
	
	static Console.Size console_size = null;
	static Console console;
	
	static KeyListener key_listener;
	
	//	Build build = new Build();
	JsonObject json_character;
	ArraySelection<Item> equipment; // TODO implement custom class - needed for used/unused items
	List<Selection<?>> selections = new ArrayList<>();
	
	Main() throws FileNotFoundException {
//		json_character = loadJSON("character.json").getAsJsonObject()
//		json_character = loadJSON("character_qq.json").getAsJsonObject()
		json_character = loadJSON("current.json").getAsJsonObject()
			.get("character").getAsJsonObject();
		
		equipment = ArraySelection.of(
			Utils.json_array_to_list(
				json_character.get("equipment").getAsJsonArray(),
//				json_character.get("inventory").getAsJsonArray(),
				Item::new
			)
		);
		selections.add(equipment);
		render();
	}
	
	private record SelectionStop(int start, int width) {}
	private static final SelectionStop[] sel_stops = new SelectionStop[]{
		new SelectionStop(0, 50), // 40
		new SelectionStop(50, 70),
		new SelectionStop(120, 50),
	};
	
	void highlight_start(boolean do_start) {
		if (!do_start) return;
		console.out.print(format_negative);
		console.out.print(color_back(0, 0, 0));
	}
	void highlight_end(boolean do_end) {
		if (!do_end) return;
		console.out.print(format_positive);
		console.out.print(format_reset_back);
	}
	
	void render() {
		console_size = console.get_size();
		console.out.print(clear);
		
		console.out.println("console_size: " + console_size);
		console.out.println("selected: " + selections.getLast().selected());
		final int start_i = 2;
		
		//console.out.print(color_back(50, 50, 50));
		//for (int i = 0; i < 8; i++) {
		//	console.out.print(cursor_position(sel_stops[2].start, i + start_i));
		//	console.out.print(" ".repeat(sel_stops[2].width));
		//}
		//console.out.print(format_reset);
		
		// selections
		for (int depth = 0; depth < selections.size(); depth++) {
			int i = start_i, j = 0;
			Selection<?> sel = selections.get(depth);
			SelectionStop stop = sel_stops[depth];
			
			for (Render element : sel) {
				highlight_start(j == sel.selected());
				for (String line : element.render(stop.width).split("\n"))
					console.out.print(cursor_position(stop.start, i++) + line);
				highlight_end(j == sel.selected());
				j++;
			}
		}
		console.out.print(format_reset);
		
		// next preview
		if (selections.getLast().select() instanceof Selection<?> next_sel) {
			SelectionStop stop = sel_stops[selections.size()];
			int i = start_i;
			for (Render element : next_sel)
				for (String line : element.render(stop.width).split("\n"))
					console.out.print(cursor_position(stop.start, i++) + line);
		}
		console.out.print(format_reset);
		
		// stats
		if (selections.getLast().select() instanceof Item item) {
			for (int i = 0; i < item.mods.size(); i++) {
				console.out.print(cursor_position(sel_stops[2].start, i + start_i));
				Modifier mod = item.mods.get(i);
				console.out.format(
					" %1$-" + (sel_stops[2].width - 2) + "s ",
					mod.interpretations.get(mod.selected_interpretation)
				);
			}
		}
		console.out.print(cursor_position(0, console_size.height() - 1));
	}
	
	private static PoeAPI api = new PoeAPI();
	private static JsonObject get_current_character() throws InterruptedException {
		var json = api.request("/character").getAsJsonObject();
		
		JsonArray characters = json.getAsJsonArray("characters");
		for (var element : characters) {
			var character = element.getAsJsonObject();
			if (character.has("current"))
				return character;
		}
		return null;
	}
	private void load_character_equipment() throws InterruptedException {
		JsonObject c = get_current_character();
		assert c != null;
		JsonObject json = api.request("/character/" + c.get("name").getAsString()).getAsJsonObject();
		json_character = json.get("character").getAsJsonObject();
		equipment = ArraySelection.of(
			Utils.json_array_to_list(
				json_character.get("equipment").getAsJsonArray(),
				Item::new
			)
		);
		selections.clear();
		selections.add(equipment);
		
		render();
	}
	
	public static void main(String[] args) throws IOException, InterruptedException {
		if (!api.auth_expiry().isPositive())
			api.authorize();
		
		console = new Console();
//		console.cursor_show(false);
		key_listener = new KeyListener();
		try {
			key_listener.run(new Main());
		} finally {
			console.close();
		}
	}
}

// TODO modifier can have multiple lines
//    - each modifier starts with "-" so that the multiline mods are distinguishable

// TODO modifier should have indication whether it has only one interpretation
//    - if it has more you will see it in selection
//    - use modifier color

// TODO modifier may need color for whether it is good/bad
//    - in Path of Building the modifiers are blue/light blue
//      probably depending on their type
//    - the actual effects on the build (stats) have green/red color
//      depending on whether the stat is positive or negative (0 stats are not shown)

// TODO Path of Building works as following
//    - Item has modifiers
//    - modifiers have effective stats
//    - list of modifiers and cumulative list of stats is shown when hovering the item
//    -
//    - also (some) stats are always shown on the left
//    -
//    - Toex (console)
//    - will probably have two pages
//    - one with list of items
//      - their list of modifiers
//      - [maybe] effective stats on the far right
//      - if the modifier is selected only it's own stats will be shown (useful for debugging)
//    this makes me think that i should not think in sense of selections but rather in the actual usage
//    the [maybe] point seems to be good but the current selection is contra-productive to that
//    I should rather implement it in following way
//    {
//      List<Item> equipment;
//      key-bind L -> load_player() { character_json = PoeApi(); equipment = character_json.equipment }
//    }
//    Item { List<Modifier> mods; }
//    Modifier { ? stats }
//    equipment collect ( modifier collect stats ) -> List<Stat>

// TODO acceptable keys
//  with num-lock off
//  7 8      [ switch page ] [   up   ]
//  4 5 6    [     left    ] [ accept ] [ right ]
//  1 2      [   decline   ] [  down  ]
