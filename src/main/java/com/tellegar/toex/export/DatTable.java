package com.tellegar.toex.export;

import com.tellegar.toex.Utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * Represents content.ggpk dat file.
 * <br>
 * Implements functionality like <a href="https://snosme.github.io/poe-dat-viewer/">https://snosme.github.io/poe-dat-viewer/</a>
 * <br>
 * Using the <a href="https://github.com/poe-tool-dev/dat-schema">schema</a> for columns
 * <br>
 * note: <a href="https://youtu.be/XNN0RLIfg-Y?t=2689">developer's internal tool</a>
 */
public class DatTable {
	private final JsonObject schema;
	public int row_count;
	public int column_count;
	
	private final ByteBuffer raw;
	private final Map<String, Integer> col_by_name;
	private final int row_size;
	private int data_offset;
	private final int[] col_start;
	
	private static JsonObject schemas = null;
	private static Map<String, JsonObject> schema_by_name;
	private static void load_schemas() throws FileNotFoundException {
		schemas = Utils.loadJSON(Export.resources + "schema.min.json").getAsJsonObject();
		schema_by_name = new HashMap<>();
		JsonArray schemas_tables = schemas.get("tables").getAsJsonArray();
		for (int i = 0; i < schemas_tables.size(); i++) {
			JsonObject schema = schemas_tables.get(i).getAsJsonObject();
			schema_by_name.put(schema.get("name").getAsString().toLowerCase(), schemas_tables.get(i).getAsJsonObject());
		}
	}
	
	private static final Map<String, Integer> type_size = Map.of(
		"bool", 1,
		"string", 8,
		"i32", 4,
		"f32", 4,
		"enumrow", 4,
		"row", 8,
		"foreignrow", 16
	);
	private int get_type_size(JsonObject column_schema) {
		return column_schema.get("array").getAsBoolean() ?
			16 : type_size.get(column_schema.get("type").getAsString());
	}
	
	/**
	 * @param name Dat file name, ignores case
	 * @return {@link DatTable} for specified name
	 */
	public static DatTable from_name(String name) throws FileNotFoundException {
		String filename = name;
		if (name.contains("."))
			name = name.substring(0, name.indexOf(".")).toLowerCase();
		else
			filename += ".dat*";
		
		if (schemas == null)
			load_schemas();
		JsonObject schema = schema_by_name.get(name.toLowerCase());
		
		List<Path> matched_files = new ArrayList<>();
		try (DirectoryStream<Path> dirStream = Files.newDirectoryStream(
			Path.of(Export.export_path + "data/"), filename
//			Path.of(Export.resources), filename
		)) { // TODO switch to Export.resources
			for (Path p : dirStream)
				matched_files.add(p);
		} catch (FileNotFoundException e) {
			throw e;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		Optional<Path> dat64 = matched_files.stream().filter(p -> p.toString().endsWith(".dat64")).findAny();
		DatTable dt;
		if (dat64.isPresent())
			dt = new DatTable(dat64.get(), schema);
		else if (!matched_files.isEmpty())
			dt = new DatTable(matched_files.getFirst(), schema);
		else
			throw new RuntimeException(name + " not found");
		return dt;
	}
	
	private DatTable(Path path, JsonObject schema) {
		String filename = path.getFileName().toString();
		String extension = filename.split("\\.")[1];
		assert extension.equals("dat64") : "currently only supporting dat64"; // datl64 has utf-32 encoding
		
		this.schema = schema;
		
		int raw_size;
		try (RandomAccessFile file = new RandomAccessFile(path.toFile(), "r")) {
			FileChannel channel = file.getChannel();
			raw_size = (int) channel.size();
			raw = ByteBuffer.allocate(raw_size);
			raw.order(ByteOrder.LITTLE_ENDIAN);
			channel.read(raw);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		final int b = 4; // first 4 bytes are row_count
		row_count = raw.getInt(0);
		data_offset = -1; // self.raw.find(b"\xBB" * 8)
		for (int i = b, j = 0; i < raw_size; i++) { // simple kmp, find \xBB * 8
			if (raw.get(i) == (byte) 0xBB)
				j++;
			else
				j = 0;
			if (j == 8)
				data_offset = i - j + 1;
		}
		row_size = (data_offset - b) / row_count;
		
		// apply schema
		col_start = new int[this.schema.get("columns").getAsJsonArray().size()];
		col_start[0] = 4;
		col_by_name = new HashMap<>();
		JsonArray schema_columns = this.schema.get("columns").getAsJsonArray();
		column_count = schema_columns.size();
		int specified_size = 0;
		for (int col = 0; col < schema_columns.size(); col++) {
			JsonObject schema_column = schema_columns.get(col).getAsJsonObject();
			int size = get_type_size(schema_column);
			
			if (col != schema_columns.size() - 1)
				col_start[col + 1] = col_start[col] + size;
			
			specified_size += size;
			if (!schema_column.get("name").isJsonNull())
				col_by_name.put(schema_column.get("name").getAsString().toLowerCase(), col);
		}
		assert specified_size == row_size : "schema columns doesn't specify whole row";
	}
	
	/**
	 * Represents reference type, {@code enumrow}, {@code row} or {@code foreignrow}
	 */
	public static class Key {
		public Integer value;
		public JsonObject references;
		public Key(Integer value, JsonElement references) {
			this.value = value;
			this.references = references.isJsonNull() ? null : references.getAsJsonObject();
		}
		@Override
		public String toString() {
			return "<" + value + ">";
		}
	}
	
	/**
	 * @param col_name name of the column like in <a href=https://snosme.github.io/poe-dat-viewer/>poe-dat-viewer</a>
	 * @param row      from [0, {@link DatTable#row_count})
	 * @return corresponding type based on column
	 */
	public Object get(String col_name, int row) {
		Integer col = col_by_name.get(col_name.toLowerCase());
		if (col == null)
			throw new IllegalArgumentException("no column `" + col_name + "`");
		return get(col, row);
	}
	/**
	 * @param col from [0, {@link DatTable#column_count})
	 * @param row from [0, {@link DatTable#row_count})
	 * @return corresponding type based on column
	 */
	public Object get(int col, int row) {
		JsonObject schema = this.schema.get("columns").getAsJsonArray().get(col).getAsJsonObject();
		int start = row * row_size + col_start[col];
		
		if (schema.get("array").getAsBoolean()) {
			long count = raw.getLong(start);
			long points_to = raw.getLong(start + 8);
			assert count < 1000 : "something may went wrong";
			
			int e_start = (int) points_to + data_offset;
			int size = type_size.get(schema.get("type").getAsString());
			
			Object[] out = new Object[(int) count];
			for (int i = 0; i < count; i++) {
				out[i] = get_value(e_start, schema);
				e_start += size;
			}
			return out;
		} else
			return get_value(start, schema);
	}
	
	private Object get_value(int start, JsonObject schema) {
		String type = schema.get("type").getAsString();
		switch (type) {
			case "bool" -> {
				return raw.get(start) != 0;
			}
			case "string" -> {
				var out = new StringBuilder();
				for (int i = raw.getInt(start) + data_offset; ; i += 2) {
					char c = raw.getChar(i);
					if (c == 0) break;
					out.append(c);
				}
				return out.toString();
			}
			case "i32" -> {
				return raw.getInt(start);
			}
			case "f32" -> {
				return raw.getFloat(start);
			}
			case "enumrow", "row", "foreignrow" -> {
				boolean is_null = true;
				for (int i = 0; i < get_type_size(schema); i++)
					if (raw.get(start + i) != (byte) 0xfe) {
						is_null = false;
						break;
					}
				Integer value = null;
				if (!is_null)
					value = raw.getInt(start); // only reads 4 bytes, row/foreignrow could theoretically be larger
				return new Key(value, schema.get("references"));
			}
		}
		assert false : "unhandled type: " + type;
		return null;
	}
	
	public static void main(String[] args) throws FileNotFoundException {
//		DatTable mods = DatTable.from_name("Mods");
//		DatTable stats = DatTable.from_name("Stats");
		load_schemas();
		DatTable dat = new DatTable(
			Path.of(Export.export_path + "data/baseitemtypes.dat64"),
			schema_by_name.get("baseitemtypes")
		);
		System.out.println(dat.schema.get("name").getAsString());
		
		for (int i = 0; i < dat.schema.get("columns").getAsJsonArray().size(); i++) {
			JsonObject schema = dat.schema.get("columns").getAsJsonArray().get(i).getAsJsonObject();
			String name = schema.get("name").isJsonNull() ? "?" : schema.get("name").getAsString();
			System.out.print(name);
			System.out.print(" ".repeat(30 - name.length()));
			String type = schema.get("type").getAsString();
			if (schema.get("array").getAsBoolean())
				type += "[]";
			System.out.print(type);
			System.out.print(" ".repeat(20 - type.length()));
			
			Object value = dat.get(i, 335);
			
			if (value.getClass().isArray())
				System.out.println(Arrays.toString((Object[]) value));
			else
				System.out.println(value);
		}
	}
}
