package com.tellegar.toex.export;

import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

/**
 * exporting data from content.ggpk, using <a href="https://github.com/zao/ooz">bun_extract_file.exe</a>
 * <br>
 */
public class Export {
	public static void main(String[] args) throws IOException, InterruptedException {
//		run_bun_extract();
		download_schema();
		copy("metadata/statdescriptions/stat_descriptions.txt", "stat_descriptions.txt");
		copy_dat("stats.dat64");
		copy_dat("mods.dat64");
		copy_dat("baseitemtypes.dat64");
		copy_dat("itemclasses.dat64");
		copy_dat("itemclasscategories.dat64");
	}
	
	public static final String export_path = "export/ggpk/";
	public static final String resources = "src/main/resources/export/";
	// TODO use resources properly
	
	private static void copy(String source, String target) throws IOException {
		Files.copy(
			Path.of(export_path + source),
			Path.of(resources + target),
			StandardCopyOption.REPLACE_EXISTING
		);
	}
	private static void copy_dat(String dat_file) throws IOException {
		copy(
			"data/" + dat_file,
			Path.of(dat_file).getFileName().toString()
		);
	}
	
	/**
	 * Downloads schema for {@link DatTable}
	 */
	private static void download_schema() throws IOException, InterruptedException {
		try (HttpClient client = HttpClient.newBuilder()
			.followRedirects(HttpClient.Redirect.NORMAL)
			.build()) {
			HttpRequest request = HttpRequest.newBuilder()
				.GET()
				.uri(URI.create("https://github.com/poe-tool-dev/dat-schema/releases/download/latest/schema.min.json"))
				.build();
			
			HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
			
			try (FileWriter file = new FileWriter(resources + "schema.min.json")) {
				file.write(response.body());
			}
		}
	}
	
	/**
	 * Clears the {@link #export_path} directory and extracts {@code metadata/} and {@code data/} folders into it.
	 */
	private static void run_bun_extract() throws IOException, InterruptedException {
		long start = System.nanoTime();
		run(
			"cmd.exe", "/c",
			"rmdir", "/s/q", export_path.replace("/", "\\")
		);
		run(
			"export/bin/bun_extract_file.exe", "extract-files", "--regex",
			"C:/Program Files (x86)/Steam/steamapps/common/Path of Exile",
			export_path.replace("/", "\\"), "^metadata.+|^data.+"
		);
		long end = System.nanoTime(); // 180s
		System.out.println((end - start) / 1_000_000_000.);
	}
	
	private static void run(String... command) throws IOException, InterruptedException {
		run(true, command);
	}
	private static void run(boolean redirect, String... command) throws IOException, InterruptedException {
		ProcessBuilder pb = new ProcessBuilder(command);
		if (redirect) pb
			.redirectOutput(ProcessBuilder.Redirect.INHERIT)
			.redirectError(ProcessBuilder.Redirect.INHERIT);
		
		Process proc = pb.start();
		Thread hook = new Thread(proc::destroy);
		Runtime.getRuntime().addShutdownHook(hook);
		proc.waitFor();
		Runtime.getRuntime().removeShutdownHook(hook);
	}
}
