package com.tellegar.toex.export;

import com.tellegar.toex.export.DatTable.Key;

import java.io.FileNotFoundException;
import java.util.*;

import static com.tellegar.toex.export.Descriptions.Scope.Range;

public class Mods {
	public static DatTable mods = null;
	private static final int stats_count = 6;
	
	public record Stats(int[] stats, Range[] ranges) {}
	public static Stats get_stats(int row) {
		List<Integer>
			indices = new ArrayList<>(),
			keys = new ArrayList<>(),
			stats = new ArrayList<>();
		
		for (int i = 1; i <= stats_count; i++) {
			Integer key = ((Key) mods.get("StatsKey" + i, row)).value;
			if (key != null && !keys.contains(key)) {
				keys.add(key);
				indices.add(i);
			}
			stats.add(key);
		}
		
		return new Stats(
			indices.stream()
				.mapToInt(i -> stats.get(i - 1))
				.toArray(),
			indices.stream()
				.map(i -> new Range(
					(Integer) mods.get("Stat" + i + "Min", row),
					(Integer) mods.get("Stat" + i + "Max", row)
				))
				.toArray(Range[]::new)
		);
	}
//	public static int[] get_stats(int row) {
//		List<Integer> indices = new ArrayList<>();
//		List<Integer> out = new ArrayList<>();
//		Set<Key> keys = new HashSet<>();
//		for (int i = 1; i <= stats_count; i++) {
//			Key key = (Key) mods.get("StatsKey" + i, row);
//			if (key.value != null)
//				if (keys.add(key))
//					indices.add(i - 1);
//			out.add(key.value);
//		}
//		return indices.stream()
//			.mapToInt(out::get)
//			.toArray();
//	}
//	public static Range[] get_stat_ranges(int row) {
//		List<Integer> indices = new ArrayList<>();
//		Set<Key> keys = new HashSet<>();
//		return IntStream
//			.rangeClosed(1, stats_count)
//			.filter(i -> ((Key) mods.get("StatsKey" + i, row)).value != null)
//			.mapToObj(i -> new Range(
//				(Integer) mods.get("Stat" + i + "Min", row),
//				(Integer) mods.get("Stat" + i + "Max", row)
//			))
//			.toArray(Range[]::new);
//	}
	
	public static void init() throws FileNotFoundException {
		mods = DatTable.from_name("Mods");
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		init();
		System.out.println(get_stats(4809));
	}
}
