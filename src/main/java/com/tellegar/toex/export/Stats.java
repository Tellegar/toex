package com.tellegar.toex.export;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

public class Stats {
	public static DatTable stats = null;
	public static Map<String, Integer> id_to_row = null;
	
	public static void init() throws FileNotFoundException {
		stats = DatTable.from_name("Stats");
		
		id_to_row = new HashMap<>();
		for (int row = 0; row < stats.row_count; row++) {
			String id = (String) stats.get("Id", row);
			Integer prev = id_to_row.put(id, row);
			assert prev == null : "duplicate Ids";
		}
	}
}
