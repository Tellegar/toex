package com.tellegar.toex.export;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static com.tellegar.toex.Utils.nop;

/**
 * Templates for modifiers
 * Represents parsed {@code metadata/statdescriptions/*} files
 */
public class Descriptions {
	List<Description> descriptions = new ArrayList<>();
	List<String> not_consumed_lines = new ArrayList<>();
	
	HashMap<String, List<Description.Generator>> hash_map = new HashMap<>();
	
	static class Description {
		int[] variables; // element is stat row
		List<Generator> generators = new ArrayList<>();
		
		class Generator {
			String language;
			Scope[] variable_scopes;
			String format_string;
			String format_pattern;
			FormatPattern[] format_patterns;
			String[] arguments;
			
			Description get_description() {
				return Description.this;
			}
			
			@Override
			public boolean equals(Object o) {
				if (this == o) return true;
				if (o instanceof Generator generator) {
					if (!Objects.equals(language, generator.language)) return false;
					if (!Arrays.equals(variable_scopes, generator.variable_scopes)) return false;
					if (!Objects.equals(format_pattern, generator.format_pattern)) return false;
					return Arrays.equals(arguments, generator.arguments);
				}
				return false;
			}
		}
		
		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o instanceof Description description) {
				if (!Arrays.equals(variables, description.variables)) return false;
				return Objects.equals(generators, description.generators);
			}
			return false;
		}
		
		/**
		 * Parses variable line and sets variables
		 *
		 * @return whether the Description should be saved
		 * @throws NullPointerException when {@link Stats#init()} was not called
		 */
		private boolean parse_variables(String line) throws NullPointerException {
			String[] arr = line.split("\\s+");
			int count = Integer.parseInt(arr[0]);
			String[] variable_ids = Arrays.copyOfRange(arr, 1, arr.length);
			assert count == variable_ids.length : "count and variables.length don't match";
			
			variables = Arrays.stream(variable_ids).mapToInt(id ->
				Stats.id_to_row.get(id)
			).toArray();
			return Arrays.stream(variable_ids).noneMatch(v -> v.startsWith("old_do_not_use_"));
		}
		
		/**
		 * Parses new {@link Generator} and prepares data structures for modifier matching
		 */
		void add_generator(String line, String language) {
			if (english_only && !"English".equals(language)) return;
			var gen = new Generator();
			
			gen.language = language;
			
			String[] arr = line.split("\\s+", variables.length + 1);
			assert arr[arr.length - 1].charAt(0) == '"' : "format_string doesn't begin with \", failed to split variable_scopes";
			String[] scopes = Arrays.copyOf(arr, variables.length);
			line = arr[arr.length - 1];
			
			assert line.chars().filter(c -> c == '"').count() == 2 : "multiple \" are not handled";
			gen.format_string = line.substring(1, line.indexOf('"', 1));
			gen.format_string = gen.format_string.replaceAll("\\\\n", "\n");
			
			line = line.substring(line.indexOf('"', 1) + 1).strip();
			if (line.isEmpty()) gen.arguments = new String[0];
			else gen.arguments = line.split("\\s+");
			
			// these generator arguments are not supported
			if (Stream.of(
				"display_indexable_support",
				"display_indexable_skill",
				"mod_value_to_item_class",
				"tree_expansion_jewel_passive",
				"passive_hash",
				"affliction_reward_type",
				"weapon_tree_unique_base_type_name"
			).anyMatch(
				bad_arg -> Arrays.asList(gen.arguments).contains(bad_arg)
			)) return;
			
			gen.format_string = format_p.matcher(gen.format_string).replaceAll(match_result -> {
				String pattern = match_result.group();
				FormatPattern fp = FormatPattern.of(pattern);
				
				// fix for generator for `local_unique_jewel_projectile_damage_+1%_per_x_dex_in_radius`
				//   "範圍內每 {0:d} 點已配置敏捷，增加 {1}% 投射物傷害"
				// -> "範圍內每 {0:d} 點已配置敏捷，增加 1% 投射物傷害"
				if (fp.index < 0 || variables.length <= fp.index)
					return pattern.substring(1, pattern.length() - 1);
				return pattern;
			});
			
			gen.variable_scopes = Arrays.stream(scopes).map(
				Scope::of
			).toArray(Scope[]::new);
			
			List<FormatPattern> format_patterns = new ArrayList<>();
			
			gen.format_pattern = gen.format_string;
			gen.format_pattern = Pattern.quote(gen.format_pattern);
			gen.format_pattern = format_p.matcher(gen.format_pattern).replaceAll(match_result -> {
				String pattern = match_result.group();
				FormatPattern fp = FormatPattern.of(pattern);
				
				format_patterns.add(fp);
				if (fp.argument != null && fp.argument.startsWith("+"))
					return "\\\\E([+-]?(?:\\\\d+(?:\\\\.\\\\d*)?|\\\\.\\\\d+))\\\\Q";
				return "\\\\E(-?(?:\\\\d+(?:\\\\.\\\\d*)?|\\\\.\\\\d+))\\\\Q";
			});
			
			gen.format_patterns = format_patterns.toArray(FormatPattern[]::new);
			
			generators.add(gen);
		}
	}
	
	/**
	 * Implements a state machine for parsing a descriptions file
	 *
	 * @throws NullPointerException when {@link Stats#init()} wasn't called before
	 */
	public static Descriptions parse_file(Path path) throws IOException {
		Descriptions desc = new Descriptions();
		
		try (BufferedReader r = Files.newBufferedReader(path, StandardCharsets.UTF_16LE)) {
			//noinspection ResultOfMethodCallIgnored
			r.read(); // eat [fe ff] bytes
			
			enum State {
				description_or_language, variables, count, generators
			}
			State state = State.description_or_language;
			String language = null;
			boolean last_description_supported = true;
			int count = 0;
			
			while (true) {
				String line = r.readLine();
				if (line == null) break;
				line = line.strip();
				if (line.isEmpty()) continue;
				
				switch (state) {
					case description_or_language -> {
						if ("description".equals(line)) {
							state = State.variables;
							language = "English";
							desc.next_description(last_description_supported);
							last_description_supported = true;
						} else if (line.startsWith("lang ")) {
							// line: lang "German"
							// language:   German
							language = line.substring(6, line.length() - 1);
							state = State.count;
						} else
							desc.not_consumed_lines.add(line);
					}
					case variables -> {
						if (!desc.current_description().parse_variables(line))
							last_description_supported = false;
						state = State.count;
					}
					case count -> {
						count = Integer.parseInt(line);
						state = State.generators;
					}
					case generators -> {
						if (count > 0) {
							count--;
							desc.current_description().add_generator(line, language);
						}
						if (count == 0)
							state = State.description_or_language;
					}
				}
			}
			desc.next_description(last_description_supported);
			desc.descriptions.removeLast();
		}
		return desc;
	}
	
	/**
	 * @return list of possible modifier interpretations, for each generator that could have created this modifier
	 */
	public List<Map<Integer, Scope>> match_modifier(String modifier) {
		List<Map<Integer, Scope>> interpretations = new ArrayList<>();
		String hash = number_p.matcher(modifier).replaceAll("{}");
		if (!hash_map.containsKey(hash)) return interpretations;
		for (var generator : hash_map.get(hash)) {
			Map<Integer, Scope> mod_stats = match_modifier_using_generator(modifier, generator);
			if (mod_stats == null) continue;
			if (interpretations.contains(mod_stats)) continue;
			interpretations.add(mod_stats);
		}
		return interpretations;
	}
	
	/**
	 * Tries to match modifier against given {@link Description.Generator}
	 * <br>
	 * Used values have to satisfy scopes
	 *
	 * @return for used variables returns {@link Value}, for unused returns the {@link Scope} of the variable
	 */
	private Map<Integer, Scope> match_modifier_using_generator(String modifier, Description.Generator generator) {
		Description description = generator.get_description();
		
		Scope[] values = new Scope[description.variables.length];
		
		Matcher matcher = Pattern.compile(generator.format_pattern).matcher(modifier);
		if (matcher.matches()) {
			for (int group = 0; group < generator.format_patterns.length; group++) {
				FormatPattern fp = generator.format_patterns[group];
				values[fp.index] = Value.of(matcher.group(group + 1));
			}
		} else return null;
		
		// fix for
//		if (!Arrays.equals(new int[]{9182}, description.variables))
		apply_arguments(values, generator.arguments, true);
		
		for (int i = 0; i < description.variables.length; i++) {
			if (values[i] == null) {
				values[i] = generator.variable_scopes[i];
			} else if (!generator.variable_scopes[i].is_satisfied((Value) values[i]))
				return null;
		}
		
		Map<Integer, Scope> mod_stat = new HashMap<>();
		for (int i = 0; i < description.variables.length; i++)
			mod_stat.put(description.variables[i], values[i]);
		return mod_stat;
	}
	
	static void apply_arguments(Scope[] values, String[] arguments, boolean reverse_direction) {
		// TODO reverse_direction needs the arguments to be reordered
		//  [x+100, x*2]  ~  [x/2, x-100]
		for (int i = 0; i < arguments.length; i++) {
			String argument = arguments[i];
			switch (argument) {
				case "reminderstring" -> i++;
				case "canonical_line" -> {}
				case "canonical_stat" -> i++; // TODO does this do something?
				default -> {
					Function<Object, BigDecimal> to_bd = obj -> switch (obj) {
						case null -> null;
						case Integer val -> BigDecimal.valueOf(val);
						case Double val -> BigDecimal.valueOf(val);
						default -> throw new RuntimeException("bad value `" + obj + "`");
					};
					Integer add = switch (argument) {
						case "multiplicative_damage_modifier" -> 100;
						case "plus_two_hundred" -> 200;
						default -> null;
					};
					BigDecimal multiply = to_bd.apply(switch (argument) {
						case "negate" -> -1;
						case "times_twenty" -> 20;
						case "times_one_point_five" -> 1.5;
						case "60%_of_value" -> .60;
						case "negate_and_double" -> -2;
						case "double" -> 2;
						default -> null;
					});
					BigDecimal divide = to_bd.apply(switch (argument) {
						case "divide_by_one_hundred" -> 100;
						case "divide_by_one_hundred_2dp" -> 100;
						case "divide_by_one_hundred_2dp_if_required" -> 100;
						case "divide_by_one_hundred_and_negate" -> -100;
						
						case "per_minute_to_per_second" -> 60;
						case "per_minute_to_per_second_0dp" -> 60;
						case "per_minute_to_per_second_1dp" -> 60;
						case "per_minute_to_per_second_2dp" -> 60;
						case "per_minute_to_per_second_2dp_if_required" -> 60;
						
						case "milliseconds_to_seconds" -> 1000;
						case "milliseconds_to_seconds_0dp" -> 1000;
						case "milliseconds_to_seconds_1dp" -> 1000;
						case "milliseconds_to_seconds_2dp" -> 1000;
						case "milliseconds_to_seconds_2dp_if_required" -> 1000;
						case "divide_by_one_thousand" -> 1000;
						
						case "deciseconds_to_seconds" -> 10;
						case "locations_to_metres" -> 10;
						case "divide_by_ten_0dp" -> 10;
						case "divide_by_ten_1dp" -> 10;
						case "divide_by_ten_1dp_if_required" -> 10;
						
						case "divide_by_twelve" -> 12;
						case "divide_by_six" -> 6;
						case "divide_by_fifteen_0dp" -> 15;
						case "divide_by_twenty_then_double_0dp" -> 10; // rounding to even number?
						case "divide_by_two_0dp" -> 2;
						case "divide_by_three" -> 3;
						case "divide_by_five" -> 5;
						case "divide_by_fifty" -> 50;
						case "divide_by_twenty" -> 20;
						case "divide_by_four" -> 4;
						default -> null;
					});
					{ // swap multiply, divide based on reverse_direction
						BigDecimal multiply_ = reverse_direction ? divide : multiply;
						BigDecimal divide_ = reverse_direction ? multiply : divide;
						multiply = multiply_;
						divide = divide_;
					}
					
					int index = Integer.parseInt(arguments[++i]) - 1;
					if (values[index] == null)
						// some descriptions have nonsensical arguments
						// # "With at least 40 Intelligence in Radius, Blight inflicts Withered for 2 seconds" milliseconds_to_seconds 1
						// 1000 "Chill Nearby Enemies when you Block" milliseconds_to_seconds 1 reminderstring ReminderTextChillNonHit
						// 	1|# "Chill Nearby Enemies when you Block" milliseconds_to_seconds 1 reminderstring ReminderTextChillNonHit
						continue; // throw new RuntimeException("values[index] is null");
					
					if (values[index] instanceof Value v) {
						BigDecimal value = v.value;
						if (add != null)
							value = value.add(BigDecimal.valueOf(reverse_direction ? -add : add));
						else if (multiply != null)
							value = value.multiply(multiply);
						else if (divide != null)
							value = value.divide(divide, 5, RoundingMode.HALF_UP);
//							value = value.divide(divide);
//						if (add != null)
//							value = reverse_direction ? value - add : value + add;
//						else if (multiply != null)
//							value = reverse_direction ? value / multiply : value * multiply;
//						else if (divide != null)
//							value = reverse_direction ? value * divide : value / divide;
						else
							assert false : "`" + argument + "` is not handled";
						values[index] = Value.of(value);
					} else
						throw new RuntimeException("values[index] not instanceof Value");
				}
			}
		}
	}
	
	/**
	 * Saves the last {@link Description} (if supported) and initializes next to be parsed
	 *
	 * @param supported Whether the last description should be saved or overwritten
	 */
	private void next_description(boolean supported) {
		assert !(!supported && descriptions.isEmpty()) : "not supported and descriptions is empty";
		if (!supported)
			descriptions.removeLast();
		else if (!descriptions.isEmpty()) {
			for (var gen : descriptions.getLast().generators) {
				String hash = hash_pattern.matcher(gen.format_string).replaceAll("{}");
				if (!hash_map.containsKey(hash))
					hash_map.put(hash, new ArrayList<>());
				hash_map.get(hash).add(gen);
			}
		}
		descriptions.add(new Description());
	}
	
	private Description current_description() {
		return descriptions.getLast();
	}
	
	static final Pattern format_p = Pattern.compile("\\{.*?[}\\]]");
	static final Pattern number_p = Pattern.compile("[+-]*(?:\\d+(?:\\.\\d*)?|\\.\\d+)");
	static final Pattern hash_pattern = Pattern.compile("[+-]?" + "\\.?" + format_p + "\\.?" + "|" + number_p);
	
	static boolean english_only = true;
	
	record FormatPattern(int index, String argument) {
		static FormatPattern of(String pattern) {
			String ptrn = pattern;
			ptrn = ptrn.substring(1, ptrn.length() - 1); // {0:+d} -> 0:+d
			String[] arr = ptrn.split(":"); // 0:+d -> [0, +d]
			assert arr.length <= 2 : "invalid format pattern \"" + pattern + "\"";
			return new FormatPattern(
				arr[0].isEmpty() ? 0 : Integer.parseInt(arr[0]),
				arr.length == 2 ? arr[1] : null
			);
		}
	}
	
	/**
	 * Represents variable scopes of modifier
	 * <blockquote><pre>
	 *    Scope Modifier
	 *     -1|1 "{0:+d} Prefix Modifier allowed"
	 *     #    "{0:+d} Prefix Modifiers allowed"
	 * </pre></blockquote>
	 * In other words, Scope is a set of possible variable values
	 */
	public interface Scope {
		record Range(Integer low, Integer high) implements Scope {
			@Override
			public String toString() {
				if (low == null && high == null)
					return "#";
				return (low == null ? "#" : low.toString()) + "|" + (high == null ? "#" : high.toString());
			}
			@Override
			public boolean is_satisfied(Value v) {
				// not low <= v <= high
				if (low != null && v.value.compareTo(BigDecimal.valueOf(low)) < 0) return false;
				if (high != null && BigDecimal.valueOf(high).compareTo(v.value) < 0) return false;
				return true;
			}
			
			/**
			 * @return checks whether Range is 0|0
			 */
			public boolean is_zero() {
				return Objects.equals(low, 0) && Objects.equals(high, 0);
			}
		}
		record NotValue(int value) implements Scope {
			@Override
			public String toString() {
				return "!" + value;
			}
			
			@Override
			public boolean is_satisfied(Value v) {
				return v.value.compareTo(BigDecimal.valueOf(value)) != 0;
			}
		}
		
		/**
		 * @param value
		 * @return whether Value is from Scope (set of Values)
		 */
		boolean is_satisfied(Value value);
		
		static Scope of(String scope) {
			if (scope.startsWith("!"))
				return new NotValue(Integer.parseInt(scope.substring(1)));
			
			// fix for:
			// 1|1|# "Lacaios têm {0}% de Mana máxima aumentada"
			// take first and last
			String[] arr = scope.split("\\|");
			return new Range(
				scope_boundary(arr[0]),
				scope_boundary(arr[arr.length - 1])
			);
		}
		private static Integer scope_boundary(String boundary) {
			return "#".equals(boundary) ? null : Integer.parseInt(boundary);
		}
	}
	
	/**
	 * Represents one specific value of variable from modifier
	 */
	public static class Value implements Scope {
		Value(BigDecimal value) {
			this.value = value;
		}
		
		static Value of(String value) {
			return new Value(new BigDecimal(value));
		}
		static Value of(BigDecimal value) {
			return new Value(value);
		}
		static Value of(int value) {
			return of(BigDecimal.valueOf(value));
		}
		static Value of(double value) {
			return of(BigDecimal.valueOf(value));
		}
		
		public BigDecimal value;
		
		@Override
		public boolean is_satisfied(Value value) {
			BigDecimal this_rounded = this.value.setScale(2, RoundingMode.HALF_UP);
			BigDecimal value_rounded = value.value.setScale(2, RoundingMode.HALF_UP);
			return this_rounded.compareTo(value_rounded) == 0;
		}
		@Override
		public String toString() {
			return String.valueOf(value);
		}
		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o instanceof Value v)
				return is_satisfied(v);
			return false;
		}
	}
	
	public static void main(String[] args) throws IOException {
		Stats.init();
		Descriptions desc = Descriptions.parse_file(Path.of(Export.resources + "stat_descriptions.txt"));
		System.out.println("parsed " + desc.descriptions.size() + " descriptions");
		System.out.println("ignored " + desc.not_consumed_lines.size() + " lines");
		
		System.out.println(desc.match_modifier("+24 to Strength and Intelligence"));
		System.out.println(desc.match_modifier("47% reduced Explicit Ailment Modifier magnitudes"));
		
		int val = 0;
		String str = null;
		for (var description : desc.descriptions)
			for (var generator : description.generators)
				for (var line : generator.format_string.split("\\n"))
					if (val < line.length()) {
						val = line.length();
						str = line;
					}
		System.out.println("val: " + val);
		System.out.println(str);
		
		nop();
	}
}
