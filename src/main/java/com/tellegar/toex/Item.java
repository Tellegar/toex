package com.tellegar.toex;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.tellegar.toex.export.DatTable;
import com.tellegar.toex.user_interface.EscapeSequences;
import com.tellegar.toex.user_interface.Render;
import com.tellegar.toex.user_interface.Selection;

import java.io.FileNotFoundException;
import java.util.*;

public class Item implements Render, Selection<Modifier> {
	JsonObject item;
	final List<Modifier> mods;
	int selected_mod = 0;
	
	Item(JsonElement item) {
		this.item = item.getAsJsonObject();
		mods = get_mods();
	}
	
	private final static DatTable baseItemTypes, itemClasses, itemClassCategories;
	private final static Map<String, String> category_by_baseType = new HashMap<>();
	
	static {
		try {
			baseItemTypes = DatTable.from_name("BaseItemTypes");
			itemClasses = DatTable.from_name("ItemClasses");
			itemClassCategories = DatTable.from_name("ItemClassCategories");
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
		for (int row = 0; row < baseItemTypes.row_count; row++) {
			String name = (String) baseItemTypes.get("Name", row);
			DatTable.Key key_item_class = (DatTable.Key) baseItemTypes.get(
				"ItemClassesKey",
				row
			);
			DatTable.Key key_category = (DatTable.Key) itemClasses.get(
				"ItemClassCategory",
				key_item_class.value
			);
			if (key_category.value == null) continue;
			String category = (String) itemClassCategories.get("Text", key_category.value);
			category_by_baseType.put(name, category);
		}
	}
	
	// TODO ArmourTypes->IncreasedMovementSpeed hidden stat for category "Body Armour"
	
	// TODO weapons should have only one category (so that the category indicates inventory slot)
	//      WeaponClasses->ItemClass - ItemClasses keys
	//      WeaponTypes->BaseItemTypeKey - BaseItemTypes keys
	String get_category() {
		String baseType = item.get("baseType").getAsString();
		return category_by_baseType.get(baseType);
	}
	
	String get_name() {
		String name = item.get("name").getAsString();
		if (name.isEmpty()) return item.get("typeLine").getAsString();
		return name;
	}
	String get_rarity() {
		return item.has("rarity") ? item.get("rarity").getAsString() : null;
	}
	
	Color get_color() {
		String rarity = get_rarity();
		return switch (rarity) {
			case "Normal" -> new Color(200, 200, 200);
			case "Magic" -> new Color(136, 136, 255);
			case "Rare" -> new Color(255, 255, 119);
			case "Unique" -> new Color(175, 96, 37);
			case null -> null;
			default -> throw new RuntimeException("rarity: `" + rarity + "` is not handled");
		};
	}
	
	boolean identified() {
		return item.get("identified").getAsBoolean();
	}
	
	// interface Render
	@Override
	public String render(int width) {
		String out = String.format(" %1$-" + (width - 2) + "s ", get_name());
		Color c = get_color();
		if (c != null) out = c.color_text() + out;
		
		String suffix_note = "[" + get_category() + "] ";
		out = out.substring(0, out.length() - suffix_note.length()) + suffix_note;
		
		return out;
	}
	
	// interface Selection<Modifier>
	@Override
	public int selected() {
		return selected_mod;
	}
	@Override
	public void select(int selected) {
		selected_mod = Math.clamp(selected, 0, mods.size() - 1);
	}
	@Override
	public Modifier select() {
		return mods.get(selected_mod);
	}
	@Override
	// selecting between one modifier should be possible
	public boolean is_selectable() {
		return !mods.isEmpty();
	}
	@Override
	public Iterator<Modifier> iterator() {
		if (!identified()) {
			return Collections.singleton(
				(Modifier) new Modifier("", "") {
					@Override
					public String render(int width) {
						return " " + EscapeSequences.color_text(210, 0, 0) + "Unidentified";
					}
				}
			).iterator();
		}
		return mods.iterator();
	}
	
	private List<Modifier> get_mods() {
		List<Modifier> mods = new ArrayList<>();
		for (String mod_type : List.of(
			"implicitMods",
			"explicitMods",
			"craftedMods",
			"enchantMods",
			"scourgeMods",
			"ultimatumMods",
			"crucibleMods"
		)) {
			if (!item.has(mod_type)) continue;
			for (var modifier : item.get(mod_type).getAsJsonArray())
				mods.add(new Modifier(
					modifier.getAsString(),
					mod_type.substring(0, mod_type.length() - 4)
				));
		}
		return mods;
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		JsonObject character_json = Utils
			.loadJSON("character.json").getAsJsonObject()
			.get("character").getAsJsonObject();
		
		List<Item> equipment = new ArrayList<>();
		for (var e : character_json.get("equipment").getAsJsonArray())
			equipment.add(new Item(e));
		
		for (var item : equipment)
			System.out.println(item.get_name());
		System.out.println();
		
		List<Modifier> mods = equipment.getFirst().get_mods();
		
		for (var mod : mods)
			System.out.println(mod);
		System.out.println();
	}
}
